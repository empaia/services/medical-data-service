# Medical Data Service

## Dev Setup

* install docker
* install docker-compose
* install poetry
* clone medical-data-service (with `git clone --recurse-submodules`)

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd medical-data-service
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Set Environment

Set environment variables in a `.env` file.

```bash
cp sample.env .env  # edit .env if necessary
```

For running the tests, download the OpenSlide_adapted folder from nextcloud. It contains the WSIs 
needed by the tests. Set the value of the `COMPOSE_TEST_DATA` variable in `.env` to the path of local
folder.


### Run

Start services using `docker-compose`.

```bash
docker-compose up --build -d
```

Or start services with uvicorn

```bash
uvicorn --host=0.0.0.0 --port=5000 medical_data_service.app:app
```

### Stop and Remove

```bash
docker-compose down
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest` 

```bash
isort .
black medical_data_service tests
pycodestyle medical_data_service tests
pylint medical_data_service tests
# use sample.env
pytest tests/non_auth --maxfail=1
# use sample_auth.env
pytest tests/auth --maxfail=1
```
