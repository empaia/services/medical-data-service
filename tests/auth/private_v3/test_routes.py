import json

import requests

from .settings import Settings

settings = Settings()
mds_url = settings.mds_url.strip("/")


EXCLUDED_PATHS = []


def test_routes():
    r = requests.get(f"{mds_url}/private/v3/openapi.json")
    openapi = json.loads(r.content)
    for path, ops in openapi["paths"].items():
        if path in EXCLUDED_PATHS:
            continue

        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{mds_url}/private/v3{path_no_vars}"
        for op in ops.keys():
            print("url", url)
            print("op", op)
            r = requests.request(op, url)
            print("r.text", r.text)
            print("r.status", r.status_code)
            assert r.status_code == 403
