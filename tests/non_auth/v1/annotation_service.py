import json

import requests

from .settings import Settings


class AnnotationService:
    settings = Settings()

    def create_point_annotation(
        self,
        name,
        job_id,
        slide_id,
    ):
        endpoint = "/v1/annotations"
        point_annotation = {
            "name": name,
            "creator_id": job_id,
            "creator_type": "job",
            "reference_id": slide_id,
            "reference_type": "wsi",
            "type": "point",
            "npp_created": "1",
            "coordinates": [100, 200],
        }
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(point_annotation))
        print(r.status_code)
        print(r.text)
        assert r.status_code == 201
        return r.json()["id"]

    def create_class(self, name, job_id, annotation_id, value="org.empaia.my_vendor.my_app.v1.classes.non_tumor"):
        endpoint = "/v1/classes"
        class_example = {
            "creator_id": job_id,
            "creator_type": "job",
            "reference_id": annotation_id,
            "reference_type": "annotation",
            "value": value,
            "type": "class",
        }
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(class_example))
        print(r.status_code)
        print(r.text)
        assert r.status_code == 201
        return r.json()["id"]

    def create_collection(
        self,
        name,
        job_id,
        slide_id,
    ):
        endpoint = "/v1/collections"
        collection = {
            "name": name,
            "creator_id": job_id,
            "creator_type": "job",
            "reference_id": slide_id,
            "reference_type": "wsi",
            "item_type": "float",
            "items": [],
            "type": "collection",
        }
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(collection))
        assert r.status_code == 201
        return r.json()["id"]

    def create_float_primitive(self, name, job_id, ref_id, ref_type="wsi"):
        endpoint = "/v1/primitives"
        float_primitive = {
            "name": name,
            "creator_id": job_id,
            "creator_type": "job",
            "reference_id": ref_id,
            "reference_type": ref_type,
            "type": "float",
            "value": 1.23,
        }
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(float_primitive))
        assert r.status_code == 201
        return r.json()["id"]

    def read(self, object_id, route="annotations", shallow=False):
        endpoint = f"/v1/{route}/{object_id}"
        url = self.settings.mds_url + endpoint
        if route == "collections":
            r = requests.get(url, params={"shallow": shallow})
        else:
            r = requests.get(url)
        print({"shallow": shallow})
        print(r.content)
        assert r.status_code == 200
        return r.json()

    def read_all(self, limit=None, skip=None, route="annotations"):
        endpoint = f"/v1/{route}"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def count(self, query):
        endpoint = "/v1/annotations/query/count"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def query(self, query, route="annotations"):
        endpoint = f"/v1/{route}/query"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def query_viewer(self, query, route="annotations"):
        endpoint = f"/v1/{route}/query/viewer"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def query_unique_class_values(self, query, route="annotations"):
        endpoint = f"/v1/{route}/query/unique-class-values"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def position(self, item_id, query, route="annotations"):
        endpoint = f"/v1/{route}/{item_id}/query"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def update(self, object_id, data, status_code=200, route="annotations"):
        endpoint = f"/v1/{route}/{object_id}"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(data))
        assert r.status_code == status_code
        return r.json()

    def delete(self, object_id, status_code=200, route="annotations"):
        endpoint = f"/v1/{route}/{object_id}"
        url = self.settings.mds_url + endpoint
        r = requests.delete(url)
        assert r.status_code == status_code
        return r.json()

    def lock(self, item_id, job_id, route="annotations"):
        endpoint = f"/v1/jobs/{job_id}/lock/{route}/{item_id}"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps({}))
        assert r.status_code == 200

    def query_unique_references(self, route="annotations", collection_id=None):
        if collection_id:
            endpoint = f"/v1/collections/{collection_id}/items/query/unique-references"
        else:
            endpoint = f"/v1/{route}/query/unique-references"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps({}))
        assert r.status_code == 200
        return r.json()

    def add_items_to_collection(self, collection_id, items):
        endpoint = f"/v1/collections/{collection_id}/items"
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(items))
        assert r.status_code == 201
        return r.json()

    def query_items_from_collection(self, collection_id, query):
        endpoint = f"/v1/collections/{collection_id}/items/query"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def delete_item_from_collection(self, collection_id, item_id):
        endpoint = f"/v1/collections/{collection_id}/items/{item_id}"
        url = self.settings.mds_url + endpoint
        r = requests.delete(url)
        assert r.status_code == 200

    def tree_items(self, job_id):
        endpoint = f"/v1/jobs/{job_id}/tree/items"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200

    def tree_primitives(self, job_id):
        endpoint = f"/v1/jobs/{job_id}/tree/primitives"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200

    def tree_primitives_details(self, job_id):
        endpoint = f"/v1/jobs/{job_id}/tree/primitives"
        url = self.settings.mds_url + endpoint
        primitives = requests.get(url).json()
        assert len(primitives["items"]) > 0
        for p in primitives["items"]:
            endpoint = f"/v1/jobs/{job_id}/tree/primitives/{p['id']}/details"
            url = self.settings.mds_url + endpoint
            r = requests.get(url)
            assert r.status_code == 200
            details = r.json()
            ref_type = details["reference_type"]
            if ref_type is None:
                assert details["reference_data"] is None
            elif ref_type == "collection":
                assert details["reference_data"] is not None

    def tree_node_items(self, job_id, node_type, node_id):
        endpoint = f"/v1/jobs/{job_id}/tree/nodes/{node_type}/{node_id}/items"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200

    def tree_node_sequence(self, job_id, item_id):
        endpoint = f"/v1/jobs/{job_id}/tree/items/{item_id}/sequence"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200

    def tree_node_primitives(self, job_id, node_type, node_id):
        endpoint = f"/v1/jobs/{job_id}/tree/nodes/{node_type}/{node_id}/primitives"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
