import time
import uuid

import jwt
import pytest
import requests

from .annotation_service import AnnotationService
from .clinical_data_service import ClinicalDataService, check_image_format
from .examination_service import ExaminationService
from .job_service import JobService
from .settings import Settings


@pytest.fixture(scope="session", autouse=True)
def wait_for_services():
    settings = Settings()
    for _ in range(60):
        try:
            r = requests.get(settings.mds_url + "/alive")
            if r.json()["status"] == "ok":
                break
        except requests.exceptions.RequestException:
            pass
        time.sleep(1)


def test_alive():
    settings = Settings()
    r = requests.get(settings.mds_url + "/alive")
    print(r.status_code)
    print(r.text)
    assert r.status_code == 200
    data = r.json()
    assert data["status"] == "ok"
    assert "version" in data
    for _, service_response in data["services"].items():
        assert service_response["status"] == "ok"
        assert "version" in service_response


def test_clinical_data_service():
    clinical_data_service = ClinicalDataService()
    # Create Case
    case_id = clinical_data_service.create_case()["id"]
    # Create Case with external id
    external_id = str(uuid.uuid4())
    case_id = clinical_data_service.create_case(external_id)["id"]
    assert external_id == case_id
    # Read Case
    case = clinical_data_service.read_case(case_id)
    assert case["id"] == case_id
    # Update Case
    case = clinical_data_service.update_case(case_id, {"description": "some description"})
    assert case["description"] == "some description"
    # Create Slide
    slide_id = clinical_data_service.add_slide(case_id)["id"]
    # Create Slide with external id
    external_id = str(uuid.uuid4())
    slide_id = clinical_data_service.add_slide(case_id, external_id)["id"]
    assert external_id == slide_id
    # Read Slide
    slide = clinical_data_service.read_slide(slide_id)
    assert slide["id"] == slide_id
    # Update Slide
    slide = clinical_data_service.update_slide(slide_id, {"stain": "HER2"})
    assert slide["id"] == slide_id
    assert slide["stain"] == "HER2"
    # Get Cases
    cases = clinical_data_service.get_cases()
    assert len(cases) > 0
    # Query Slides
    slides = clinical_data_service.get_slides()
    assert len(slides) > 0
    # Query Slides
    slides = clinical_data_service.query_slide({"cases": [case_id]})
    assert len(slides) > 0
    # Tag definitions
    tags = clinical_data_service.get_tags()
    assert tags["TISSUE"]["SKIN"] == {"DE": "Haut", "EN": "Skin"}


def test_storage_mapping():
    clinical_data_service = ClinicalDataService()
    case_slide_ids = clinical_data_service.create_case_with_slide()
    slide_id = clinical_data_service.create_storage("Aperio/CMU-1.svs", case_slide_ids["slide_id"])
    # Read
    storage_address = clinical_data_service.read_storage(slide_id)
    assert storage_address["slide_id"] == slide_id
    # Delete
    clinical_data_service.delete_storage(slide_id)
    with pytest.raises(AssertionError):
        storage_address = clinical_data_service.read_storage(slide_id)


def test_clinical_data_service_wsi_data():
    clinical_data_service = ClinicalDataService()
    case_slide_ids = clinical_data_service.create_case_with_slide()
    slide_id = clinical_data_service.create_storage("Aperio/CMU-1.svs", case_slide_ids["slide_id"])

    slide_info = clinical_data_service.get_slide_info(slide_id)
    assert slide_info["extent"]["x"] == 46000
    assert slide_info["extent"]["y"] == 32914

    slide_label = clinical_data_service.get_slide_label(slide_id)
    check_image_format(slide_label, "JPEG", (84, 100))
    slide_label = clinical_data_service.get_slide_label(slide_id, params={"image_format": "png"})
    check_image_format(slide_label, "PNG", (84, 100))

    clinical_data_service.get_slide_thumbnail(slide_id)
    clinical_data_service.get_slide_macro(slide_id)
    clinical_data_service.get_slide_region(slide_id)
    clinical_data_service.get_slide_tile(slide_id)


def test_job_service():
    clinical_data_service = ClinicalDataService()
    case_slide_ids = clinical_data_service.create_case_with_slide()
    slide_id = clinical_data_service.create_storage("Aperio/CMU-1.svs", case_slide_ids["slide_id"])

    job_service = JobService()

    app_id = job_service.create_ead()
    job_id = job_service.create_job(app_id=app_id)["id"]
    job_service.update_job_input(job_id=job_id, slide_id=slide_id)
    job = job_service.read_job(job_id)
    assert job["id"] == job_id

    job_service.update_job_status(job_id, "SCHEDULED")
    job = job_service.read_job(job_id)
    assert job["status"] == "SCHEDULED"

    res = job_service.query_jobs(statuses=["RUNNING"])
    assert res["item_count"] == 0
    res = job_service.query_jobs(statuses=["SCHEDULED"])

    job_position = None
    for pos, job in enumerate(res["items"]):
        if job["id"] == job_id:
            job_position = pos
            break

    assert job_position is not None
    assert job_position == 0

    job_service.update_job_output(job_id=job_id)
    job_service.get_public_key()

    job_id = job_service.create_job(app_id=app_id)["id"]
    job_service.delete_job(job_id=job_id)

    job_id = job_service.create_job(app_id=app_id, creator_type="SCOPE")["id"]
    job_service.delete_job(job_id=job_id)

    job_id = job_service.create_job(app_id=app_id)["id"]
    res = job_service.get_token(job_id=job_id)
    assert res["job_id"] == job_id
    assert "access_token" in res

    job_id = job_service.create_job(app_id=app_id)["id"]
    job_service.update_job_input(job_id=job_id, slide_id=slide_id)
    job_service.delete_job_input(job_id=job_id)


def test_examination_service():
    clinical_data_service = ClinicalDataService()
    case = clinical_data_service.create_case()
    case_id = case["id"]

    examination_service = ExaminationService()
    # Create Examination
    examination = examination_service.create_examination(case_id)
    examination_id = examination["id"]
    # Read Examination
    examination = examination_service.read_examination(examination_id)
    assert examination["case_id"] == case_id
    examinations = examination_service.read_examinations()
    assert len(examinations["items"]) == examinations["item_count"]
    job_id = str(uuid.uuid4())
    app_id = str(uuid.uuid4())
    # Update Examination with job and state data
    examination_service.add_app(examination_id, app_id)
    examination_service.add_job(examination_id, app_id, job_id)
    examination = examination_service.read_examination(examination_id)
    jobs = examination_service.read_jobs(examination, app_id)
    assert len(jobs) == 1
    assert jobs[0] == job_id
    examination_service.update_state(examination_id, "OPEN")
    examination = examination_service.read_examination(examination_id)
    assert examination["apps"][0]["id"] == app_id
    assert examination["state"] == "OPEN"
    # Get examination by job id
    ex_temp = examination_service.read_examination_by_job_id(job_id)
    assert ex_temp["id"] == examination_id
    # Query Examinations
    examinations_query = examination_service.query_examination(case_id)
    assert examinations_query["item_count"] == 1
    # Delete job
    new_job_id = str(uuid.uuid4())
    examination_service.add_job(examination_id, app_id, new_job_id)
    examination = examination_service.read_examination(examination_id)
    jobs = examination_service.read_jobs(examination, app_id)
    assert len(jobs) == 2
    examination_service.delete_job(examination_id, new_job_id)
    examination = examination_service.read_examination(examination_id)
    jobs = examination_service.read_jobs(examination, app_id)
    assert jobs is not None
    assert len(jobs) == 1
    # Create scope, get scope, and get scope data
    user_id = str(uuid.uuid4())
    response_data = examination_service.create_scope(examination_id, app_id, user_id)
    assert "id" in response_data
    response_data = examination_service.get_scope(examination_id, app_id, user_id)
    assert "id" in response_data
    scope_id = response_data["id"]
    scope_data = examination_service.get_scope_data(response_data["id"])
    assert scope_data["id"] == response_data["id"]
    assert scope_data["examination_id"] == examination_id
    assert scope_data["app_id"] == app_id
    assert scope_data["user_id"] == user_id
    # Test that the scope access token can be decoded using the public key
    scope_access_token = examination_service.get_scope_token(scope_id)
    public_key = examination_service.get_public_key()
    payload = jwt.decode(scope_access_token, key=public_key, algorithms="RS256")
    assert payload["sub"] == scope_id
    # Close examination, try adding job and app, delete job
    examination_service.update_state(examination_id, "CLOSED")
    examination_service.add_job(examination_id, app_id, new_job_id, closed=True)
    examination_service.add_app(examination_id, str(uuid.uuid4()), closed=True)
    examination_service.delete_job(examination_id, job_id, closed=True)


def test_annotation_service():
    clinical_data_service = ClinicalDataService()
    case_slide_ids = clinical_data_service.create_case_with_slide()
    slide_id = clinical_data_service.create_storage("Aperio/CMU-1.svs", case_slide_ids["slide_id"])

    job_service = JobService()
    app_id = job_service.create_ead()
    job_id = job_service.create_job(app_id=app_id)["id"]
    job_service.update_job_input(job_id=job_id, slide_id=slide_id)
    job_service.update_job_status(job_id, "SCHEDULED")

    annotation_service = AnnotationService()
    annotation_name = "my_result"
    # Create Annotation
    annotation_id = annotation_service.create_point_annotation(annotation_name, job_id, slide_id)
    # Read Annotation
    annotation_example = annotation_service.read(annotation_id)
    # Create Class
    class_id = annotation_service.create_class(annotation_name, job_id, annotation_id)
    # Read Class
    class_example = annotation_service.read(class_id, route="classes")
    # Create Collection
    collection_id = annotation_service.create_collection(annotation_name, job_id, slide_id)
    # Read Collection
    collection = annotation_service.read(collection_id, route="collections")
    # Create Primitive
    primitive_id = annotation_service.create_float_primitive(annotation_name, job_id, slide_id)
    # Read Primitive
    primitive = annotation_service.read(primitive_id, route="primitives")
    # Query Annotations
    query = {
        "viewport": {"x": 1, "y": 1, "width": 2, "height": 2},
    }
    annotations = annotation_service.query(query)
    assert annotations["item_count"] == 0
    query = {
        "viewport": {"x": 99, "y": 199, "width": 2, "height": 2},
    }
    annotations = annotation_service.query(query)
    assert annotations["item_count"] > 0
    annot_id = annotations["items"][0]["id"]
    annotation_service.position(annot_id, query)
    query = {"viewport": {"x": 99, "y": 199, "width": 2, "height": 2}, "npp_viewing": [1, 1000000]}
    annotation_viewer_result = annotation_service.query_viewer(query)
    assert len(annotation_viewer_result["annotations"]) > 0
    assert "low_npp_centroids" in annotation_viewer_result
    query["annotations"] = annotation_viewer_result["annotations"]
    annotations = annotation_service.query(query)
    assert annotations["item_count"] > 0
    count = annotation_service.count(query)
    assert count["item_count"] > 0
    # Query unique class values
    new_point_annotation_id = annotation_service.create_point_annotation(annotation_name, job_id, slide_id)
    annotation_service.create_class(annotation_name, job_id, new_point_annotation_id, value="my_class")
    query = {}
    unique_class_values = annotation_service.query_unique_class_values(query)
    assert len(unique_class_values["unique_class_values"]) > 0
    for route, annotation in [
        ("annotations", annotation_example),
        ("classes", class_example),
        ("collections", collection),
        ("primitives", primitive),
    ]:
        # Update
        if route == "collections":
            del annotation["items"]
        annotation = annotation_service.read(annotation["id"], route=route)
        # Delete
        item_count_before = annotation_service.read_all(route=route)["item_count"]
        annotation_service.delete(annotation["id"], route=route)
        item_count = annotation_service.read_all(route=route)["item_count"]
        assert item_count == item_count_before - 1
        # Lock
        if route == "annotations":
            new_annotation_id = annotation_service.create_point_annotation(annotation_name, job_id, slide_id)
        if route == "classes":
            new_point_annotation_id = annotation_service.create_point_annotation(annotation_name, job_id, slide_id)
            new_annotation_id = annotation_service.create_class(annotation_name, job_id, new_point_annotation_id)
        if route == "collections":
            new_annotation_id = annotation_service.create_collection(annotation_name, job_id, slide_id)
        if route == "primitives":
            new_annotation_id = annotation_service.create_float_primitive(annotation_name, job_id, slide_id)
        annotation_service.lock(new_annotation_id, job_id, route=route)
        annotation = annotation_service.read(new_annotation_id, route=route)
        if route == "collections":
            del annotation["items"]
        annotation_service.delete(new_annotation_id, status_code=423, route=route)  # Locked
    # Add items to collection
    collection_id = annotation_service.create_collection(annotation_name, job_id, slide_id)
    items = {
        "items": [
            {
                "name": "item_0",
                "creator_id": job_id,
                "creator_type": "job",
                "reference_id": slide_id,
                "reference_type": "wsi",
                "type": "float",
                "value": 1.23,
            }
        ]
    }
    annotation_service.add_items_to_collection(collection_id, items)
    # Query items from collection
    query = {"references": [slide_id]}
    items = annotation_service.query_items_from_collection(collection_id, query)
    assert items["item_count"] == 1
    # Read Collection Shallow
    collection = annotation_service.read(collection_id, route="collections", shallow=False)
    assert len(collection["items"]) == 1
    collection_shallow = annotation_service.read(collection_id, route="collections", shallow=True)
    assert len(collection_shallow["items"]) == 0
    # Query unique references
    unique_references_annots = annotation_service.query_unique_references(route="annotations")
    assert len(unique_references_annots["wsi"]) > 0
    unique_references_classes = annotation_service.query_unique_references(route="classes")
    assert len(unique_references_classes["annotation"]) > 0
    unique_references_prims = annotation_service.query_unique_references(route="primitives")
    assert len(unique_references_prims["wsi"]) > 0
    unique_references_colls = annotation_service.query_unique_references(route="collections")
    assert len(unique_references_colls["wsi"]) > 0
    unique_references_items = annotation_service.query_unique_references(collection_id=collection_id)
    assert len(unique_references_items["wsi"]) > 0
    # Delete item from collection
    annotation_service.delete_item_from_collection(collection_id, items["items"][0]["id"])
    items = annotation_service.query_items_from_collection(collection_id, query)
    assert items["item_count"] == 0
    # Lock collection
    annotation_service.lock(collection_id, job_id, route="collections")
    # Lock slide
    annotation_service.lock(slide_id, job_id, route="slides")
    # Tree
    float_id_no_ref = annotation_service.create_float_primitive("p_no_ref", job_id, None, None)
    float_id_col_ref = annotation_service.create_float_primitive("p_col_ref", job_id, collection_id, "collection")
    annotation_service.lock(float_id_no_ref, job_id, route="primitives")
    annotation_service.lock(float_id_col_ref, job_id, route="primitives")
    annotation_service.tree_items(job_id)
    annotation_service.tree_primitives(job_id)
    annotation_service.tree_primitives_details(job_id)
    annotation_service.tree_node_items(job_id, "wsi", slide_id)
    # Lock annotation
    annotation_id = annotation_service.create_point_annotation(annotation_name, job_id, slide_id)
    annotation_service.lock(annotation_id, job_id, route="annotations")
    annotation_service.tree_node_sequence(job_id, annotation_id)
    annotation_service.tree_node_primitives(job_id, "wsi", slide_id)
    # Update Job by adding output information
    job_service.update_job_output(job_id=job_id, output_key=annotation_name, output_id=annotation_id)
    job = job_service.read_job(job_id)
    assert job["outputs"][annotation_name] == annotation_id
