import uuid

import requests

from .settings import Settings


class JobService:
    settings = Settings()

    def create_ead(self):
        app_id = str(uuid.uuid4())
        ead = {
            "$schema": "ead-schema.v1-draft3.json",
            "namespace": "org.empaia.test_vendor.test_app.v1",
            "name": "Test App",
            "name_short": "Test App",
            "description": "This is a test app",
            "inputs": {"my_wsi": {"type": "wsi"}},
            "outputs": {"my_result": {"type": "point", "reference": "inputs.my_wsi"}},
        }
        endpoint = f"/v1/apps/{app_id}/ead"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, json=ead)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        return app_id

    def create_job(self, app_id, creator_type="USER"):
        endpoint = "/v1/jobs"
        job_example = {
            "app_id": app_id,
            "creator_id": "not_defined",
            "creator_type": creator_type,
        }
        url = self.settings.mds_url + endpoint
        r = requests.post(url, json=job_example)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        return r.json()

    def update_job_input(self, job_id, slide_id):
        endpoint = f"/v1/jobs/{job_id}/inputs/my_wsi"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, json={"id": slide_id})
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        assert isinstance(r.json(), dict)

    def delete_job_input(self, job_id):
        endpoint = f"/v1/jobs/{job_id}/inputs/my_wsi"
        url = self.settings.mds_url + endpoint
        r = requests.delete(url)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        assert isinstance(r.json(), dict)

    def read_job(self, job_id):
        endpoint = f"/v1/jobs/{job_id}"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        return r.json()

    def query_jobs(self, statuses=None):
        endpoint = "/v1/jobs/query"
        url = self.settings.mds_url + endpoint
        data = {"statuses": statuses}
        r = requests.put(url, json=data)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        return r.json()

    def update_job_status(self, job_id, status):
        endpoint = f"/v1/jobs/{job_id}/status"
        url = self.settings.mds_url + endpoint
        data = {"status": status}
        r = requests.put(url, json=data)
        assert r.status_code == 200

    def update_job_output(self, job_id, output_key="my_result", output_id=str(uuid.uuid4())):
        endpoint = f"/v1/jobs/{job_id}/outputs/{output_key}"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, json={"id": output_id})
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200

    def get_public_key(self):
        endpoint = "/v1/job-service/public-key"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def delete_job(self, job_id):
        endpoint = f"/v1/jobs/{job_id}"
        url = self.settings.mds_url + endpoint
        r = requests.delete(url)
        assert r.status_code == 200

    def get_token(self, job_id):
        endpoint = f"/v1/jobs/{job_id}/token"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()
