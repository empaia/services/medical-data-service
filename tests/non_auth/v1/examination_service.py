import uuid

import requests

from .settings import Settings


class ExaminationService:
    settings = Settings()

    def create_examination(self, case_id):
        examination_example = {
            "case_id": case_id,
            "creator_id": str(uuid.uuid4()),
            "creator_type": "USER",
        }
        return self._post("/v1/examinations", expected_status_code=201, json=examination_example)

    def read_examination(self, examination_id):
        return self._get(f"/v1/examinations/{examination_id}", expected_status_code=200)

    def query_examination(self, case_id):
        return self._put("/v1/examinations/query", expected_status_code=200, json={"cases": [case_id]})

    def read_examinations(self):
        return self._get("/v1/examinations", expected_status_code=200)

    def add_job(self, examination_id, app_id, job_id, closed=False):
        self._put(
            f"/v1/examinations/{examination_id}/apps/{app_id}/jobs/{job_id}/add",
            expected_status_code=423 if closed else 200,
        )

    def read_examination_by_job_id(self, job_id):
        return self._get(f"/v1/jobs/{job_id}/examinations", expected_status_code=200)

    def add_app(self, examination_id, app_id, closed=False):
        self._put(f"/v1/examinations/{examination_id}/apps/{app_id}/add", expected_status_code=423 if closed else 200)

    def update_state(self, examination_id, state):
        data = {"state": state}
        self._put(f"/v1/examinations/{examination_id}/state", expected_status_code=200, json=data)

    def delete_job(self, examination_id, job_id, closed=False):
        self._delete(f"/v1/examinations/{examination_id}/jobs/{job_id}", expected_status_code=423 if closed else 200)

    def read_jobs(self, examination, app_id):
        for examination_app in examination["apps"]:
            if examination_app["id"] == app_id:
                jobs = examination_app["jobs"]
                assert jobs is not None
                return jobs

    def create_scope(self, examination_id, app_id, user_id, expected_status_code=200):
        data = {"examination_id": examination_id, "app_id": app_id, "user_id": user_id}
        return self._post("/v1/scopes", expected_status_code, json=data)

    def get_scope(self, examination_id, app_id, user_id, expected_status_code=200):
        data = {"examination_id": examination_id, "app_id": app_id, "user_id": user_id}
        return self._put("/v1/scopes", expected_status_code, json=data)

    def get_scope_data(self, scope_id, expected_status_code=200):
        return self._get(f"/v1/scopes/{scope_id}", expected_status_code)

    def get_scope_token(self, scope_id, expected_status_code=200):
        return self._get(f"/v1/scopes/{scope_id}/token", expected_status_code)["access_token"].encode("ascii")

    def get_public_key(self, expected_status_code=200):
        return self._get("/v1/examination-service/public-key", expected_status_code).encode("ascii")

    def _delete(self, endpoint, expected_status_code, **args):
        r = requests.delete(self.settings.mds_url + endpoint, **args)
        self._assert_status_code(r, expected_status_code, endpoint)
        return r

    def _get(self, endpoint, expected_status_code, **args):
        r = requests.get(self.settings.mds_url + endpoint, **args)
        self._assert_status_code(r, expected_status_code, endpoint)
        return r.json()

    def _post(self, endpoint, expected_status_code, **args):
        r = requests.post(self.settings.mds_url + endpoint, **args)
        self._assert_status_code(r, expected_status_code, endpoint)
        return r.json()

    def _put(self, endpoint, expected_status_code, **args):
        r = requests.put(self.settings.mds_url + endpoint, **args)
        self._assert_status_code(r, expected_status_code, endpoint)
        return r.json()

    @staticmethod
    def _assert_status_code(response, expected_status_code, endpoint):
        if response.status_code != expected_status_code:
            raise AssertionError(
                f"Expected status code {expected_status_code}, "
                f"but got {response.status_code} instead. Endpoint: {endpoint}."
            )
