import requests

from .settings import Settings


class BaseService:
    settings = Settings()

    def _delete(self, endpoint, expected_status_code, **args):
        r = requests.delete(self.settings.mds_url + endpoint, **args)
        self._assert_status_code(r, expected_status_code, endpoint)
        return r

    def _get(self, endpoint, expected_status_code, **args):
        r = requests.get(self.settings.mds_url + endpoint, **args)
        self._assert_status_code(r, expected_status_code, endpoint)
        return r.json()

    def _post(self, endpoint, expected_status_code, **args):
        r = requests.post(self.settings.mds_url + endpoint, **args)
        self._assert_status_code(r, expected_status_code, endpoint)
        return r.json()

    def _put(self, endpoint, expected_status_code, **args):
        r = requests.put(self.settings.mds_url + endpoint, **args)
        print(r.json())
        self._assert_status_code(r, expected_status_code, endpoint)
        return r.json()

    @staticmethod
    def _assert_status_code(response: requests.Response, expected_status_code, endpoint):
        if response.status_code != expected_status_code:
            raise AssertionError(
                f"Expected status code {expected_status_code}, "
                f"but got {response.status_code} instead. Endpoint: {endpoint}."
            )
