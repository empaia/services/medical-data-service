import gzip
import json
import uuid
from pprint import pprint
from typing import List, Optional

import numpy as np
import requests

from medical_data_service.models.v3.annotation.pixelmaps import PixelmapReferenceType
from medical_data_service.models.v3.commons import DataCreatorType

from .settings import Settings


class AnnotationService:
    settings = Settings()

    def create_point_annotation(
        self,
        name,
        job_id,
        slide_id,
    ):
        endpoint = "/v3/annotations"
        point_annotation = {
            "name": name,
            "creator_id": job_id,
            "creator_type": "job",
            "reference_id": slide_id,
            "reference_type": "wsi",
            "type": "point",
            "npp_created": "1",
            "coordinates": [100, 200],
        }
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(point_annotation))
        print(r.status_code)
        print(r.text)
        assert r.status_code == 201
        return r.json()["id"]

    def create_class(self, name, job_id, annotation_id, value="org.empaia.my_vendor.my_app.v1.classes.non_tumor"):
        endpoint = "/v3/classes"
        class_example = {
            "creator_id": job_id,
            "creator_type": "job",
            "reference_id": annotation_id,
            "reference_type": "annotation",
            "value": value,
            "type": "class",
        }
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(class_example))
        print(r.status_code)
        print(r.text)
        assert r.status_code == 201
        return r.json()["id"]

    def create_collection(
        self,
        name,
        job_id,
        slide_id,
    ):
        endpoint = "/v3/collections"
        collection = {
            "name": name,
            "creator_id": job_id,
            "creator_type": "job",
            "reference_id": slide_id,
            "reference_type": "wsi",
            "item_type": "float",
            "items": [],
            "type": "collection",
        }
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(collection))
        assert r.status_code == 201
        return r.json()["id"]

    def create_float_primitive(self, name, job_id, ref_id, ref_type="wsi"):
        endpoint = "/v3/primitives"
        float_primitive = {
            "name": name,
            "creator_id": job_id,
            "creator_type": "job",
            "reference_id": ref_id,
            "reference_type": ref_type,
            "type": "float",
            "value": 1.23,
        }
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(float_primitive))
        assert r.status_code == 201
        return r.json()["id"]

    def read(self, object_id, route="annotations", shallow=False):
        endpoint = f"/v3/{route}/{object_id}"
        url = self.settings.mds_url + endpoint
        if route == "collections":
            r = requests.get(url, params={"shallow": shallow})
        else:
            r = requests.get(url)
        print({"shallow": shallow})
        print(r.content)
        assert r.status_code == 200
        return r.json()

    def read_all(self, limit=None, skip=None, route="annotations"):
        endpoint = f"/v3/{route}"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def count(self, query):
        endpoint = "/v3/annotations/query/count"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def query(self, query, route="annotations"):
        endpoint = f"/v3/{route}/query"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def query_viewer(self, query, route="annotations"):
        endpoint = f"/v3/{route}/query/viewer"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def query_unique_class_values(self, query, route="annotations"):
        endpoint = f"/v3/{route}/query/unique-class-values"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def position(self, item_id, query, route="annotations"):
        endpoint = f"/v3/{route}/{item_id}/query"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def update(self, object_id, data, status_code=200, route="annotations"):
        endpoint = f"/v3/{route}/{object_id}"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(data))
        assert r.status_code == status_code
        return r.json()

    def delete(self, object_id, status_code=200, route="annotations"):
        endpoint = f"/v3/{route}/{object_id}"
        url = self.settings.mds_url + endpoint
        r = requests.delete(url)
        assert r.status_code == status_code
        return r.json()

    def lock(self, item_id, job_id, route="annotations"):
        endpoint = f"/v3/jobs/{job_id}/lock/{route}/{item_id}"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps({}))
        assert r.status_code == 200

    def query_unique_references(self, route="annotations", collection_id=None):
        if collection_id:
            endpoint = f"/v3/collections/{collection_id}/items/query/unique-references"
        else:
            endpoint = f"/v3/{route}/query/unique-references"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps({}))
        assert r.status_code == 200
        return r.json()

    def add_items_to_collection(self, collection_id, items):
        endpoint = f"/v3/collections/{collection_id}/items"
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(items))
        assert r.status_code == 201
        return r.json()

    def query_items_from_collection(self, collection_id, query):
        endpoint = f"/v3/collections/{collection_id}/items/query"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(query))
        assert r.status_code == 200
        return r.json()

    def delete_item_from_collection(self, collection_id, item_id):
        endpoint = f"/v3/collections/{collection_id}/items/{item_id}"
        url = self.settings.mds_url + endpoint
        r = requests.delete(url)
        assert r.status_code == 200

    def get_nominal_pixelmap(
        self,
        tilesize: int,
        level: dict,
        element_class_mapping: List[dict],
        element_type: str = "int8",
        channel_count: int = 1,
        neutral_value: Optional[int] = None,
        reference_id: Optional[str] = None,
        creator_id: Optional[str] = None,
        creator_type: str = DataCreatorType.JOB,
    ):
        if reference_id is None:
            reference_id = str(uuid.uuid4())

        if creator_id is None:
            creator_id = str(uuid.uuid4())

        return {
            "type": "nominal_pixelmap",
            "name": "NOMINAL PIXELMAP",
            "tilesize": tilesize,
            "element_type": element_type,
            "channel_count": channel_count,
            "levels": [level],
            "element_class_mapping": element_class_mapping,
            "reference_id": reference_id,
            "reference_type": PixelmapReferenceType.WSI,
            "creator_id": creator_id,
            "creator_type": creator_type,
            "description": None,
            "neutral_value": neutral_value,
        }

    def create_pixelmap(self, pixelmap):
        endpoint = "/v3/pixelmaps"
        url = self.settings.mds_url + endpoint
        r = requests.post(url, json=pixelmap)
        pprint(r.json())
        assert r.status_code == 201
        return r.json()

    def get_pixelmap_info(self, pixelmap_id):
        endpoint = f"/v3/pixelmaps/{pixelmap_id}/info"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def put_tile(
        self,
        pixelmap_id: str,
        level: int,
        tile_x: int,
        tile_y: int,
        data: bytes,
        expected_status_code: int = 204,
        compress=False,
    ):
        endpoint = f"/v3/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
        url = self.settings.mds_url + endpoint
        headers = None
        if compress:
            data = gzip.compress(data=data, compresslevel=1)
            headers = {"Content-Encoding": "gzip"}
        r = requests.put(url, data=data, headers=headers)
        assert r.status_code == expected_status_code

    def get_tile(
        self, pixelmap_id: str, level: int, tile_x: int, tile_y: int, expected_status_code: int = 200, decompress=False
    ):
        endpoint = f"/v3/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
        url = self.settings.mds_url + endpoint
        headers = None
        if decompress:
            headers = {"Accept-Encoding": "gzip "}
        r = requests.get(url, headers=headers)
        assert r.status_code == expected_status_code
        data = r.content

        if decompress:
            # data is decompressed automatically by requests
            encoding_header = r.headers.get("Content-Encoding")
            assert encoding_header is not None
            assert encoding_header == "gzip"

        return data

    def put_tiles(
        self,
        pixelmap_id: str,
        level: int,
        start_x: int,
        start_y: int,
        end_x: int,
        end_y: int,
        data: bytes,
        compress=False,
    ):
        endpoint = (
            f"/v3/pixelmaps/{pixelmap_id}/level/{level}/position" f"/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data"
        )
        url = self.settings.mds_url + endpoint
        headers = None
        if compress:
            data = gzip.compress(data=data, compresslevel=1)
            headers = {"Content-Encoding": "gzip"}
        r = requests.put(url, data=data, headers=headers)
        assert r.status_code == 204

    def get_tiles(
        self, pixelmap_id: str, level: int, start_x: int, start_y: int, end_x: int, end_y: int, decompress=False
    ):
        endpoint = (
            f"/v3/pixelmaps/{pixelmap_id}/level/{level}/position" f"/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data"
        )
        url = self.settings.mds_url + endpoint
        headers = None
        if decompress:
            headers = {"Accept-Encoding": "gzip"}
        r = requests.get(url, headers=headers)
        assert r.status_code == 200
        data = r.content

        if decompress:
            # data is decompressed automatically by requests
            encoding_header = r.headers.get("Content-Encoding")
            assert encoding_header is not None
            assert encoding_header == "gzip"

        return data

    def slice_bulk_data(self, pixelmap: dict, tiles_data: bytes, start_x: int, start_y: int, end_x: int, end_y: int):
        retrieved_tiles = []

        tiles = np.frombuffer(tiles_data, dtype=pixelmap["element_type"])
        tile_length = (pixelmap["tilesize"] ** 2) * pixelmap["channel_count"]
        tile_count = (end_x - start_x + 1) * (end_y - start_y + 1)

        start_idx = 0
        current_x = start_x
        current_y = start_y
        for _ in range(tile_count):
            tile = tiles[start_idx : start_idx + tile_length]

            tile = tile.reshape((pixelmap["channel_count"], pixelmap["tilesize"], pixelmap["tilesize"]))

            start_idx += tile_length
            if current_x == end_x:
                current_x = start_x
                current_y += 1
            else:
                current_x += 1
            retrieved_tiles.append(tile)

        return retrieved_tiles
