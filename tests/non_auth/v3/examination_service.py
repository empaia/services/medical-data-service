import uuid

from tests.non_auth.v3.base_service import BaseService

from .settings import Settings


class ExaminationService(BaseService):
    settings = Settings()

    def create_or_get_open_examination(self, case_id, app_id, expected_status_code=201):
        examination_example = {
            "case_id": case_id,
            "creator_id": str(uuid.uuid4()),
            "creator_type": "USER",
            "app_id": app_id,
        }
        return self._put("/v3/examinations", expected_status_code=expected_status_code, json=examination_example)

    def read_examination(self, examination_id):
        return self._get(f"/v3/examinations/{examination_id}", expected_status_code=200)

    def query_examination(self, case_id):
        return self._put("/v3/examinations/query", expected_status_code=200, json={"cases": [case_id]})

    def read_examinations(self):
        return self._get("/v3/examinations", expected_status_code=200)

    def add_job(self, examination_id, job_id, expected_status_code=201):
        self._put(
            f"/v3/examinations/{examination_id}/jobs/{job_id}/add",
            expected_status_code=expected_status_code,
        )

    def update_state(self, examination_id, state):
        data = {"state": state}
        self._put(f"/v3/examinations/{examination_id}/state", expected_status_code=200, json=data)

    def delete_job(self, examination_id, job_id, closed=False):
        self._delete(f"/v3/examinations/{examination_id}/jobs/{job_id}", expected_status_code=423 if closed else 200)

    def create_or_get_scope(self, examination_id, user_id, expected_status_code=201):
        data = {"examination_id": examination_id, "user_id": user_id}
        return self._put("/v3/scopes", expected_status_code, json=data)

    def get_scope_data(self, scope_id, expected_status_code=200):
        return self._get(f"/v3/scopes/{scope_id}", expected_status_code)

    def query_scope(self, scope_id):
        return self._put("/v3/scopes/query", expected_status_code=200, json={"scopes": [scope_id]})

    def get_scope_token(self, scope_id, expected_status_code=200):
        return self._get(f"/v3/scopes/{scope_id}/token", expected_status_code)["access_token"].encode("ascii")

    def get_public_key(self, expected_status_code=200):
        return self._get("/v3/examinations/public-key", expected_status_code).encode("ascii")

    def create_preprocessing_trigger(self, creator_id, portal_app_id, stain, tissue, expected_status_code=201):
        data = {
            "creator_id": creator_id,
            "creator_type": "USER",
            "portal_app_id": portal_app_id,
            "stain": stain,
            "tissue": tissue,
        }
        return self._post("/v3/preprocessing-triggers", expected_status_code, json=data)

    def get_preprocessing_triggers(self, expected_status_code=200):
        return self._get("/v3/preprocessing-triggers", expected_status_code)

    def get_preprocessing_trigger(self, trigger_id, expected_status_code=200):
        return self._get(f"/v3/preprocessing-triggers/{trigger_id}", expected_status_code)

    def delete_preprocessing_trigger(self, trigger_id, expected_status_code=204):
        return self._delete(f"/v3/preprocessing-triggers/{trigger_id}", expected_status_code)

    def create_preprocessing_request(self, creator_id, slide_id, expected_status_code=201):
        data = {
            "creator_id": creator_id,
            "creator_type": "USER",
            "slide_id": slide_id,
        }
        return self._post("/v3/preprocessing-requests", expected_status_code, json=data)

    def put_scope_app_ui_storage(self, scope_id, storage_content):
        data = {"content": storage_content}
        return self._put(f"/v3/scopes/{scope_id}/app-ui-storage/scope", 201, json=data)

    def put_user_app_ui_storage(self, scope_id, storage_content):
        data = {"content": storage_content}
        return self._put(f"/v3/scopes/{scope_id}/app-ui-storage/user", 201, json=data)

    def get_scope_app_ui_storage(self, scope_id):
        return self._get(f"/v3/scopes/{scope_id}/app-ui-storage/scope", 200)

    def get_user_app_ui_storage(self, scope_id):
        return self._get(f"/v3/scopes/{scope_id}/app-ui-storage/user", 200)

    def get_preprocessing_request(self, request_id, expected_status_code=200):
        return self._get(f"/v3/preprocessing-requests/{request_id}", expected_status_code)

    def query_preprocessing_requests(self, states, expected_status_code=200):
        query = {"states": [states]}
        return self._put("/v3/preprocessing-requests/query", expected_status_code, json=query)

    def process_preprocessing_request(self, request_id, app_jobs, expected_status_code=200):
        update = {"app_jobs": app_jobs}
        return self._put(f"/v3/preprocessing-requests/{request_id}/process", expected_status_code, json=update)
