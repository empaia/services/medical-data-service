import json
import os
import shutil
import tempfile
import uuid

import requests
from PIL import Image

from .settings import Settings


class ClinicalDataService:
    settings = Settings()

    def download(self, slide_id, output_folder):
        def download_file(url, download_folder):
            with requests.get(url, stream=True) as r:
                r.raise_for_status()
                filename = r.headers["content-disposition"].replace("attachment;filename=", "")
                file_path = os.path.join(download_folder, filename)
                with open(file_path, "wb") as f:
                    for chunk in r.iter_content(chunk_size=1_000_000):
                        f.write(chunk)
            return file_path

        endpoint = "/v3/slides/{slide_id}/download"
        url = self.settings.mds_url + endpoint.format(slide_id=slide_id)
        return download_file(url, output_folder)

    def create_case_with_slide(self, external_case_id: str = None, external_slide_id: str = None):
        case = self.create_case(external_id=external_case_id)
        slide = self.add_slide(case_id=case["id"], external_id=external_slide_id)
        return {"case_id": case["id"], "slide_id": slide["id"]}

    def create_case(self, external_id=None, indication=None, procedure=None):
        endpoint = "/private/v3/cases"
        url = self.settings.mds_url + endpoint
        params = {}
        case_example = {"creator_id": "test_user", "creator_type": "USER"}

        if external_id:
            params["external_ids"] = True
            case_example["id"] = external_id

        if indication is not None:
            case_example["indication"] = indication

        if procedure is not None:
            case_example["procedure"] = procedure

        r = requests.post(url, json=case_example, params=params)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        return r.json()

    def read_case(self, case_id):
        endpoint = "/v3/cases/{case_id}"
        url = self.settings.mds_url + endpoint.format(case_id=case_id)
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def update_case(self, case_id, case_dict):
        endpoint = "/private/v3/cases/{case_id}"
        url = self.settings.mds_url + endpoint.format(case_id=case_id)
        r = requests.put(url, data=json.dumps(case_dict))
        assert r.status_code == 200
        return r.json()

    def get_cases(self):
        endpoint = "/v3/cases"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def query_case(self, case_dict):
        endpoint = "/v3/cases/query"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(case_dict))
        assert r.status_code == 200
        return r.json()

    def add_slide(self, case_id, external_id=None):
        endpoint = "/private/v3/slides"
        slide_data = {"case_id": case_id}
        if external_id:
            params = {"external_ids": True}
            slide_data = {"id": external_id, "case_id": case_id}
        else:
            params = {}
            slide_data = {"case_id": case_id}
        url = self.settings.mds_url + endpoint
        r = requests.post(url, data=json.dumps(slide_data), params=params)
        assert r.status_code == 200
        return r.json()

    def read_slide(self, slide_id):
        endpoint = "/v3/slides/{slide_id}"
        url = self.settings.mds_url + endpoint.format(slide_id=slide_id)
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def update_slide(self, slide_id, slide_dict):
        endpoint = "/private/v3/slides/{slide_id}"
        url = self.settings.mds_url + endpoint.format(slide_id=slide_id)
        r = requests.put(url, data=json.dumps(slide_dict))
        print(r.content)
        assert r.status_code == 200
        return r.json()

    def get_slides(self):
        endpoint = "/v3/slides"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def query_slide(self, slide_dict):
        endpoint = "/v3/slides/query"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, data=json.dumps(slide_dict))
        assert r.status_code == 200
        return r.json()

    def get_tags(self):
        endpoint = "/private/v3/tags"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def create_storage(self, storage_adress, slide_id):
        endpoint = f"/private/v3/slides/{slide_id}/storage"
        slide_storage_info = {"main_storage_address": {"storage_address_id": str(uuid.uuid4()), "path": storage_adress}}
        url = self.settings.mds_url + endpoint
        print(url)
        r = requests.put(url, data=json.dumps(slide_storage_info))
        print(r.content)
        assert r.status_code == 200
        return slide_id

    def read_storage(self, slide_id):
        endpoint = "/private/v3/slides/{slide_id}/storage"
        url = self.settings.mds_url + endpoint.format(slide_id=slide_id)
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def delete_storage(self, slide_id):
        endpoint = "/private/v3/slides/{slide_id}/storage"
        url = self.settings.mds_url + endpoint.format(slide_id=slide_id)
        r = requests.delete(url)
        assert r.status_code == 200

    def get_slide_info(self, slide_id):
        endpoint = "/v3/slides/{slide_id}/info"
        url = self.settings.mds_url + endpoint.format(slide_id=slide_id)
        r = requests.get(url)
        print(r.text)
        assert r.status_code == 200
        return r.json()

    def get_slide_label(self, slide_id, params=None):
        if params is None:
            params = {"image_format": "jpg", "image_quality": 90}
        endpoint = "/v3/slides/{slide_id}/label/max_size/{max_x}/{max_y}"
        url = self.settings.mds_url + endpoint.format(slide_id=slide_id, max_x=100, max_y=100)
        r = requests.get(url, stream=True, params=params)
        assert r.status_code == 200
        return r.raw

    def get_slide_thumbnail(self, slide_id, params=None):
        if params is None:
            params = {"image_format": "jpg", "image_quality": 90}
        endpoint = "/v3/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}"
        url = self.settings.mds_url + endpoint.format(slide_id=slide_id, max_x=100, max_y=100)
        r = requests.get(url, stream=True, params=params)
        assert r.status_code == 200
        return r.raw

    def get_slide_macro(self, slide_id, params=None):
        if params is None:
            params = {"image_format": "jpg", "image_quality": 90}
        endpoint = "/v3/slides/{slide_id}/macro/max_size/{max_x}/{max_y}"
        url = self.settings.mds_url + endpoint.format(slide_id=slide_id, max_x=100, max_y=100)
        r = requests.get(url, stream=True, params=params)
        assert r.status_code == 200
        return r.raw

    def get_slide_region(self, slide_id, params=None):
        if params is None:
            params = {"image_format": "jpg", "image_quality": 90, "z": 0}
        endpoint = "/v3/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}"
        url = self.settings.mds_url + endpoint.format(
            slide_id=slide_id, level=0, start_x=0, start_y=0, size_x=10, size_y=10
        )
        r = requests.get(url, stream=True, params=params)
        assert r.status_code == 200
        return r.raw

    def get_slide_tile(self, slide_id, params=None):
        if params is None:
            params = {"image_format": "jpg", "image_quality": 90, "z": 0}
        endpoint = "/v3/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}"
        url = self.settings.mds_url + endpoint.format(slide_id=slide_id, level=0, tile_x=0, tile_y=0)
        r = requests.get(url, stream=True, params=params)
        assert r.status_code == 200
        return r.raw


def check_image_format(image_raw, image_format, image_size):
    with tempfile.NamedTemporaryFile() as tmp_file:
        shutil.copyfileobj(image_raw, tmp_file)
        assert Image.open(tmp_file).format == image_format
        assert Image.open(tmp_file).size == image_size
