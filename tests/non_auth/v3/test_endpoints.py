import os
import shutil
import tempfile
import time
import uuid

import jwt
import numpy as np
import pytest
import requests

from medical_data_service.models.v3.annotation.pixelmaps import PixelmapSlideInfo
from tests.non_auth.v3.harpy_service import HarpyService

from .annotation_service import AnnotationService
from .clinical_data_service import ClinicalDataService, check_image_format
from .examination_service import ExaminationService
from .job_service import JobService
from .settings import Settings


@pytest.fixture(scope="session", autouse=True)
def wait_for_services():
    settings = Settings()
    for _ in range(60):
        try:
            r = requests.get(settings.mds_url + "/alive")
            if r.json()["status"] == "ok":
                break
        except requests.exceptions.RequestException:
            pass
        time.sleep(1)


def test_alive():
    settings = Settings()
    r = requests.get(settings.mds_url + "/alive")
    print(r.status_code)
    print(r.text)
    assert r.status_code == 200
    data = r.json()
    assert data["status"] == "ok"
    assert "version" in data
    for _, service_response in data["services"].items():
        assert service_response["status"] == "ok"
        assert "version" in service_response


def test_clinical_data_service():
    clinical_data_service = ClinicalDataService()
    # Create Case
    case_id = clinical_data_service.create_case()["id"]
    # Create Case with external id
    external_id = str(uuid.uuid4())
    case_id = clinical_data_service.create_case(external_id)["id"]
    assert external_id == case_id
    # Read Case
    case = clinical_data_service.read_case(case_id)
    assert case["id"] == case_id
    # Update Case
    case = clinical_data_service.update_case(case_id, {"description": "some description"})
    assert case["description"] == "some description"
    # Query Cases
    cases = clinical_data_service.query_case({"cases": [case_id]})
    assert len(cases["items"]) > 0
    # Create Slide
    slide_id = clinical_data_service.add_slide(case_id)["id"]
    # Create Slide with external id
    external_id = str(uuid.uuid4())
    slide_id = clinical_data_service.add_slide(case_id, external_id)["id"]
    assert external_id == slide_id
    # Read Slide
    slide = clinical_data_service.read_slide(slide_id)
    assert slide["id"] == slide_id
    # Update Slide
    slide = clinical_data_service.update_slide(slide_id, {"stain": "HER2"})
    assert slide["id"] == slide_id
    assert slide["stain"] == "HER2"
    # Get Cases
    cases = clinical_data_service.get_cases()
    assert len(cases) > 0
    # Query Slides
    slides = clinical_data_service.get_slides()
    assert len(slides) > 0
    # Query Slides
    slides = clinical_data_service.query_slide({"cases": [case_id]})
    assert len(slides) > 0
    # Case Tags
    indication = "ICCR_LUNG_CANCERS"
    procedure = "FINE_NEEDLE_BIOPSY"
    case = clinical_data_service.create_case(indication=indication, procedure=procedure)
    assert case["indication"] == indication
    assert case["procedure"] == procedure
    case_id = case["id"]
    case = clinical_data_service.read_case(case_id)
    assert case["indication"] == indication
    assert case["procedure"] == procedure


def test_storage_mapping():
    clinical_data_service = ClinicalDataService()
    case_slide_ids = clinical_data_service.create_case_with_slide()
    slide_id = clinical_data_service.create_storage("Aperio/CMU-1.svs", case_slide_ids["slide_id"])
    # Read
    storage_address = clinical_data_service.read_storage(slide_id)
    assert storage_address["slide_id"] == slide_id
    # Delete
    clinical_data_service.delete_storage(slide_id)
    with pytest.raises(AssertionError):
        storage_address = clinical_data_service.read_storage(slide_id)


def test_clinical_data_service_wsi_data():
    clinical_data_service = ClinicalDataService()
    case_slide_ids = clinical_data_service.create_case_with_slide()
    slide_id = clinical_data_service.create_storage("Aperio/CMU-1.svs", case_slide_ids["slide_id"])

    slide_info = clinical_data_service.get_slide_info(slide_id)
    assert slide_info["extent"]["x"] == 46000
    assert slide_info["extent"]["y"] == 32914

    tmp_dir = tempfile.mkdtemp()
    slide_file_path = clinical_data_service.download(slide_id, tmp_dir)
    assert os.path.exists(slide_file_path)
    assert os.stat(slide_file_path).st_size == 177552738
    shutil.rmtree(tmp_dir)

    slide_label = clinical_data_service.get_slide_label(slide_id)
    check_image_format(slide_label, "JPEG", (84, 100))
    slide_label = clinical_data_service.get_slide_label(slide_id, params={"image_format": "png"})
    check_image_format(slide_label, "PNG", (84, 100))

    clinical_data_service.get_slide_thumbnail(slide_id)
    clinical_data_service.get_slide_macro(slide_id)
    clinical_data_service.get_slide_region(slide_id)
    clinical_data_service.get_slide_tile(slide_id)


def test_job_service():
    clinical_data_service = ClinicalDataService()
    case_slide_ids = clinical_data_service.create_case_with_slide()
    slide_id = clinical_data_service.create_storage("Aperio/CMU-1.svs", case_slide_ids["slide_id"])

    job_service = JobService()

    app_id = str(uuid.uuid4())
    job_id = job_service.create_job(app_id=app_id)["id"]
    job_service.update_job_input(job_id=job_id, input_id=slide_id)
    job = job_service.read_job(job_id)
    assert job["id"] == job_id

    job_service.update_job_status(job_id, "SCHEDULED")
    job = job_service.read_job(job_id)
    assert job["status"] == "SCHEDULED"

    res = job_service.query_jobs_by_creator_types(creator_types=["SCOPE", "SERVICE", "USER"])
    assert res["item_count"] > 0

    res = job_service.query_jobs(statuses=["RUNNING"])
    assert res["item_count"] == 0
    res = job_service.query_jobs(statuses=["SCHEDULED"])

    job_position = None
    for pos, job in enumerate(res["items"]):
        if job["id"] == job_id:
            job_position = pos
            break

    assert job_position is not None
    assert job_position == 0

    job_service.get_public_key()

    job_id = job_service.create_job(app_id=app_id)["id"]
    job_service.delete_job(job_id=job_id)

    job_id = job_service.create_job(app_id=app_id, creator_type="SCOPE")["id"]
    job_service.delete_job(job_id=job_id)

    job_id = job_service.create_job(app_id=app_id)["id"]
    res = job_service.get_token(job_id=job_id)
    assert res["job_id"] == job_id
    assert "access_token" in res

    job_id = job_service.create_job(app_id=app_id)["id"]
    job_service.update_job_input(job_id=job_id, input_id=slide_id)
    job_service.delete_job_input(job_id=job_id)

    job_id = job_service.create_job(app_id=app_id, creator_type="SCOPE", mode="POSTPROCESSING", containerized=False)[
        "id"
    ]
    job_service.update_job_output(job_id=job_id, slide_id=slide_id)
    job_service.delete_job_output(job_id=job_id)

    app_id = str(uuid.uuid4())
    job_id = job_service.create_job(app_id=app_id, creator_type="SERVICE", mode="PREPROCESSING")["id"]
    job_service.update_job_progress(job_id, progress=0.5)
    job_service.delete_job(job_id=job_id)
    job_id = job_service.create_job(app_id=app_id, creator_type="SCOPE", mode="STANDALONE")["id"]
    job_service.delete_job(job_id=job_id)
    job_id = job_service.create_job(app_id=app_id, creator_type="SCOPE", mode="POSTPROCESSING", containerized=False)[
        "id"
    ]
    job_service.delete_job(job_id=job_id)

    job_id = job_service.create_job(app_id=app_id)["id"]
    job_service.update_validation_status("input", job_id, "RUNNING")
    job = job_service.read_job(job_id)
    assert job["input_validation_status"] == "RUNNING"
    assert not job["input_validation_error_message"]
    job_service.update_validation_status("output", job_id, "ERROR", error_message="validation failed")
    job = job_service.read_job(job_id)
    assert job["output_validation_status"] == "ERROR"
    assert job["output_validation_error_message"] == "validation failed"
    job_service.delete_job(job_id=job_id)

    app_id = str(uuid.uuid4())
    job_id = job_service.create_job(app_id=app_id, creator_type="SERVICE", mode="PREPROCESSING")["id"]
    selector_value = "org.empaia.global.v1.selectors"
    job_service.update_job_input(job_id=job_id, input_id=slide_id)
    job_service.update_job_input(job_id=job_id, input_id="12345|1", input_key="my_questionnaire")
    job_service.update_job_selector_value(job_id=job_id, input_key="my_questionnaire", selector_value=selector_value)
    res = job_service.get_job_selector_value(job_id=job_id, input_key="my_questionnaire")
    job = job_service.read_job(job_id)
    assert job["id"] == job_id
    assert res["id"] == job_id
    assert res["input_key"] == "my_questionnaire"
    assert res["selector_value"] == selector_value
    job_service.delete_job(job_id=job_id)


def test_examination_service():
    clinical_data_service = ClinicalDataService()
    case = clinical_data_service.create_case()
    case_id = case["id"]
    app_id = str(uuid.uuid4())

    examination_service = ExaminationService()
    # Create Examination
    examination = examination_service.create_or_get_open_examination(case_id, app_id)
    examination_id = examination["id"]
    # Read Examination
    examination = examination_service.read_examination(examination_id)
    assert examination["case_id"] == case_id
    assert examination["app_id"] == app_id
    examinations = examination_service.read_examinations()
    assert len(examinations["items"]) == examinations["item_count"]
    job_id = str(uuid.uuid4())
    # Update Examination with job and state data
    examination_service.add_job(examination_id, job_id)
    examination = examination_service.read_examination(examination_id)
    jobs = examination["jobs"]
    assert len(jobs) == 1
    assert jobs[0] == job_id
    examination_service.update_state(examination_id, "OPEN")
    examination = examination_service.read_examination(examination_id)
    assert examination["state"] == "OPEN"
    # Query Examinations
    examinations_query = examination_service.query_examination(case_id)
    assert examinations_query["item_count"] == 1
    # Delete job
    new_job_id = str(uuid.uuid4())
    examination_service.add_job(examination_id, new_job_id)
    examination = examination_service.read_examination(examination_id)
    jobs = examination["jobs"]
    assert len(jobs) == 2
    examination_service.delete_job(examination_id, new_job_id)
    examination = examination_service.read_examination(examination_id)
    jobs = examination["jobs"]
    assert jobs is not None
    assert len(jobs) == 1
    # Create scope, get scope, get scope data and query scope
    user_id = str(uuid.uuid4())
    response_data = examination_service.create_or_get_scope(examination_id, user_id, 201)
    assert "id" in response_data
    response_data = examination_service.create_or_get_scope(examination_id, user_id, 200)
    assert "id" in response_data
    scope_id = response_data["id"]
    scope_data = examination_service.get_scope_data(response_data["id"])
    assert scope_data["id"] == response_data["id"]
    assert scope_data["examination_id"] == examination_id
    assert scope_data["user_id"] == user_id
    scopes = examination_service.query_scope(scope_id)
    assert scopes["item_count"] == 1
    assert len(scopes["items"]) == 1
    # Test that the scope access token can be decoded using the public key
    scope_access_token = examination_service.get_scope_token(scope_id)
    public_key = examination_service.get_public_key()
    payload = jwt.decode(scope_access_token, key=public_key, algorithms="RS256")
    assert payload["sub"] == scope_id
    # Close examination, try adding job and app, delete job
    examination_service.update_state(examination_id, "CLOSED")
    examination_service.add_job(examination_id, new_job_id, 423)
    examination_service.delete_job(examination_id, job_id, closed=True)
    # Get or create examination
    examination = examination_service.create_or_get_open_examination(case_id, app_id, expected_status_code=201)
    assert examination["case_id"] == case_id
    assert examination["state"] == "OPEN"
    examination_2 = examination_service.create_or_get_open_examination(case_id, app_id, expected_status_code=200)
    assert examination_2 == examination
    # Preprocessing triggers
    creator_id = str(uuid.uuid4())
    portal_app_id = str(uuid.uuid4())
    trigger = examination_service.create_preprocessing_trigger(
        creator_id=creator_id, portal_app_id=portal_app_id, stain="HE", tissue="LUNG"
    )
    assert trigger["portal_app_id"] == portal_app_id
    trigger_list = examination_service.get_preprocessing_triggers()
    assert trigger_list["items"]
    single_trigger = examination_service.get_preprocessing_trigger(trigger["id"])
    assert single_trigger["stain"] == "HE"
    examination_service.delete_preprocessing_trigger(trigger["id"])
    # Preprocessing requests
    creator_id = str(uuid.uuid4())
    slide_id = str(uuid.uuid4())
    pp_request = examination_service.create_preprocessing_request(creator_id, slide_id)
    assert pp_request["creator_id"] == creator_id
    assert pp_request["slide_id"] == slide_id
    assert pp_request["state"] == "OPEN"
    assert pp_request["app_jobs"] == []
    get_pp_request = examination_service.get_preprocessing_request(pp_request["id"])
    assert get_pp_request == pp_request
    pp_request_list = examination_service.query_preprocessing_requests("OPEN")
    assert len(pp_request_list["items"]) >= 1
    found = False
    for r in pp_request_list["items"]:
        if r["creator_id"] == creator_id:
            assert r == pp_request
            found = True
    assert found
    app_jobs = [{"job_id": str(uuid.uuid4()), "app_id": str(uuid.uuid4()), "trigger_id": str(uuid.uuid4())}]
    examination_service.process_preprocessing_request(pp_request["id"], app_jobs)
    get_updated_pp_request = examination_service.get_preprocessing_request(pp_request["id"])
    assert get_updated_pp_request["state"] == "PROCESSED"
    assert get_updated_pp_request["app_jobs"] == app_jobs
    # scope app ui storage
    get_scope_app_storage_req = examination_service.get_scope_app_ui_storage(scope_id)
    assert get_scope_app_storage_req == {"content": {}}
    storage_content = {"a": 1, "b": True, "c": "hello"}
    put_scope_app_storage_req = examination_service.put_scope_app_ui_storage(scope_id, storage_content)
    assert put_scope_app_storage_req["content"] == storage_content
    get_scope_app_storage_req = examination_service.get_scope_app_ui_storage(scope_id)
    assert get_scope_app_storage_req == put_scope_app_storage_req
    # user app ui storage
    get_user_app_storage_req = examination_service.get_user_app_ui_storage(scope_id)
    assert get_user_app_storage_req == {"content": {}}
    storage_content = {"x": 1, "y": True, "z": "hello"}
    put_user_app_storage_req = examination_service.put_user_app_ui_storage(scope_id, storage_content)
    assert put_user_app_storage_req["content"] == storage_content
    get_user_app_storage_req = examination_service.get_user_app_ui_storage(scope_id)
    assert get_user_app_storage_req == put_user_app_storage_req


def test_annotation_service():
    clinical_data_service = ClinicalDataService()
    case_slide_ids = clinical_data_service.create_case_with_slide()
    slide_id = clinical_data_service.create_storage("Aperio/CMU-1.svs", case_slide_ids["slide_id"])

    job_service = JobService()
    app_id = str(uuid.uuid4())
    job_id = job_service.create_job(app_id=app_id)["id"]
    job_service.update_job_input(job_id=job_id, input_id=slide_id)
    job_service.update_job_status(job_id, "SCHEDULED")

    annotation_service = AnnotationService()
    annotation_name = "my_result"
    # Create Annotation
    annotation_id = annotation_service.create_point_annotation(annotation_name, job_id, slide_id)
    # Read Annotation
    annotation_example = annotation_service.read(annotation_id)
    # Create Class
    class_id = annotation_service.create_class(annotation_name, job_id, annotation_id)
    # Read Class
    class_example = annotation_service.read(class_id, route="classes")
    # Create Collection
    collection_id = annotation_service.create_collection(annotation_name, job_id, slide_id)
    # Read Collection
    collection = annotation_service.read(collection_id, route="collections")
    # Create Primitive
    primitive_id = annotation_service.create_float_primitive(annotation_name, job_id, slide_id)
    # Read Primitive
    primitive = annotation_service.read(primitive_id, route="primitives")
    # Query Annotations
    query = {
        "viewport": {"x": 1, "y": 1, "width": 2, "height": 2},
    }
    annotations = annotation_service.query(query)
    assert annotations["item_count"] == 0
    query = {
        "viewport": {"x": 99, "y": 199, "width": 2, "height": 2},
    }
    annotations = annotation_service.query(query)
    assert annotations["item_count"] > 0
    annot_id = annotations["items"][0]["id"]
    annotation_service.position(annot_id, query)
    query = {"viewport": {"x": 99, "y": 199, "width": 2, "height": 2}, "npp_viewing": [1, 1000000]}
    annotation_viewer_result = annotation_service.query_viewer(query)
    assert len(annotation_viewer_result["annotations"]) > 0
    assert "low_npp_centroids" in annotation_viewer_result
    query["annotations"] = annotation_viewer_result["annotations"]
    annotations = annotation_service.query(query)
    assert annotations["item_count"] > 0
    count = annotation_service.count(query)
    assert count["item_count"] > 0
    # Query unique class values
    new_point_annotation_id = annotation_service.create_point_annotation(annotation_name, job_id, slide_id)
    annotation_service.create_class(annotation_name, job_id, new_point_annotation_id, value="my_class")
    query = {}
    unique_class_values = annotation_service.query_unique_class_values(query)
    assert len(unique_class_values["unique_class_values"]) > 0
    for route, annotation in [
        ("annotations", annotation_example),
        ("classes", class_example),
        ("collections", collection),
        ("primitives", primitive),
    ]:
        # Update
        if route != "collections":
            annotation = annotation_service.read(annotation["id"], route=route)
            # Delete
            item_count_before = annotation_service.read_all(route=route)["item_count"]
            annotation_service.delete(annotation["id"], route=route)
            item_count = annotation_service.read_all(route=route)["item_count"]
            assert item_count == item_count_before - 1
        # Lock
        if route == "annotations":
            new_annotation_id = annotation_service.create_point_annotation(annotation_name, job_id, slide_id)
        if route == "classes":
            new_point_annotation_id = annotation_service.create_point_annotation(annotation_name, job_id, slide_id)
            new_annotation_id = annotation_service.create_class(annotation_name, job_id, new_point_annotation_id)
        if route == "collections":
            new_annotation_id = annotation_service.create_collection(annotation_name, job_id, slide_id)
        if route == "primitives":
            new_annotation_id = annotation_service.create_float_primitive(annotation_name, job_id, slide_id)
        annotation_service.lock(new_annotation_id, job_id, route=route)
        annotation = annotation_service.read(new_annotation_id, route=route)
        if route == "collections":
            del annotation["items"]
        annotation_service.delete(new_annotation_id, status_code=423, route=route)  # Locked
    # Add items to collection
    collection_id = annotation_service.create_collection(annotation_name, job_id, slide_id)
    items = {
        "items": [
            {
                "name": "item_0",
                "creator_id": job_id,
                "creator_type": "job",
                "reference_id": slide_id,
                "reference_type": "wsi",
                "type": "float",
                "value": 1.23,
            }
        ]
    }
    annotation_service.add_items_to_collection(collection_id, items)
    # Query items from collection
    query = {"references": [slide_id]}
    items = annotation_service.query_items_from_collection(collection_id, query)
    assert items["item_count"] == 1
    # Read Collection Shallow
    collection = annotation_service.read(collection_id, route="collections", shallow=False)
    assert len(collection["items"]) == 1
    collection_shallow = annotation_service.read(collection_id, route="collections", shallow=True)
    assert len(collection_shallow["items"]) == 0
    # Query unique references
    unique_references_annots = annotation_service.query_unique_references(route="annotations")
    assert len(unique_references_annots["wsi"]) > 0
    unique_references_classes = annotation_service.query_unique_references(route="classes")
    assert len(unique_references_classes["annotation"]) > 0
    unique_references_prims = annotation_service.query_unique_references(route="primitives")
    assert len(unique_references_prims["wsi"]) > 0
    unique_references_colls = annotation_service.query_unique_references(route="collections")
    assert len(unique_references_colls["wsi"]) > 0
    unique_references_items = annotation_service.query_unique_references(collection_id=collection_id)
    assert len(unique_references_items["wsi"]) > 0
    # Delete item from collection
    annotation_service.delete_item_from_collection(collection_id, items["items"][0]["id"])
    items = annotation_service.query_items_from_collection(collection_id, query)
    assert items["item_count"] == 0
    # Lock collection
    annotation_service.lock(collection_id, job_id, route="collections")
    # Lock slide
    annotation_service.lock(slide_id, job_id, route="slides")

    # Pixelmaps
    pixelmap_level = {
        "slide_level": 1,
        "position_min_x": 0,
        "position_max_x": 2,
        "position_min_y": 0,
        "position_max_y": 2,
    }
    element_class_mapping = [
        {"number_value": 0, "class_value": "org.empaia.my_vendor.my_app.v3.0.classes.zero"},
        {"number_value": 1, "class_value": "org.empaia.my_vendor.my_app.v3.0.classes.one"},
    ]
    pixelmap = annotation_service.get_nominal_pixelmap(
        tilesize=256, level=pixelmap_level, element_class_mapping=element_class_mapping, reference_id=slide_id
    )
    pixelmap = annotation_service.create_pixelmap(pixelmap=pixelmap)
    pixelmap_id = pixelmap["id"]
    pixelmap_info = annotation_service.get_pixelmap_info(pixelmap_id=pixelmap_id)
    PixelmapSlideInfo.model_validate(pixelmap_info)

    slide_info = clinical_data_service.get_slide_info(slide_id=slide_id)

    info_fields = ["extent", "num_levels", "pixel_size_nm", "levels"]

    for field in info_fields:
        assert pixelmap_info[field] == slide_info[field]

    cached_pixelmap_info = annotation_service.get_pixelmap_info(pixelmap_id=pixelmap_id)
    for field in info_fields:
        assert pixelmap_info[field] == cached_pixelmap_info[field]

    tile_data = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data[42, 42] = 10
    annotation_service.put_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, data=tile_data.tobytes())

    tile_response = annotation_service.get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)
    tile_response_data = np.frombuffer(tile_response, dtype=pixelmap["element_type"])
    tile_response_data = tile_response_data.reshape(
        (pixelmap["channel_count"], pixelmap["tilesize"], pixelmap["tilesize"])
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    tile_response = annotation_service.get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, decompress=True)
    tile_response_data = np.frombuffer(tile_response, dtype=pixelmap["element_type"])
    tile_response_data = tile_response_data.reshape(
        (pixelmap["channel_count"], pixelmap["tilesize"], pixelmap["tilesize"])
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    annotation_service.put_tile(
        pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, data=tile_data.tobytes(), compress=True
    )

    tile_response = annotation_service.get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, decompress=True)
    tile_response_data = np.frombuffer(tile_response, dtype=pixelmap["element_type"])
    tile_response_data = tile_response_data.reshape(
        (pixelmap["channel_count"], pixelmap["tilesize"], pixelmap["tilesize"])
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    tile_response = annotation_service.get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)
    tile_response_data = np.frombuffer(tile_response, dtype=pixelmap["element_type"])
    tile_response_data = tile_response_data.reshape(
        (pixelmap["channel_count"], pixelmap["tilesize"], pixelmap["tilesize"])
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    tile_data_1_1 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_1_1[1, 1] = -10
    tile_data_2_1 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_2_1[2, 1] = -5
    tile_data_1_2 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_1_2[1, 2] = 5
    tile_data_2_2 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_2_2[2, 2] = 10

    tiles = [tile_data_1_1, tile_data_2_1, tile_data_1_2, tile_data_2_2]
    byte_data = b"".join([tile.tobytes() for tile in tiles])
    annotation_service.put_tiles(
        pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2, data=byte_data
    )

    tile_response = annotation_service.get_tiles(
        pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2
    )

    retrieved_tiles = annotation_service.slice_bulk_data(
        pixelmap=pixelmap, tiles_data=tile_response, start_x=1, start_y=1, end_x=2, end_y=2
    )

    assert retrieved_tiles[0][0][1, 1] == tile_data_1_1[1, 1]
    assert retrieved_tiles[1][0][2, 1] == tile_data_2_1[2, 1]
    assert retrieved_tiles[2][0][1, 2] == tile_data_1_2[1, 2]
    assert retrieved_tiles[3][0][2, 2] == tile_data_2_2[2, 2]

    tile_response = annotation_service.get_tiles(
        pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2, decompress=True
    )

    retrieved_tiles = annotation_service.slice_bulk_data(
        pixelmap=pixelmap, tiles_data=tile_response, start_x=1, start_y=1, end_x=2, end_y=2
    )

    assert retrieved_tiles[0][0][1, 1] == tile_data_1_1[1, 1]
    assert retrieved_tiles[1][0][2, 1] == tile_data_2_1[2, 1]
    assert retrieved_tiles[2][0][1, 2] == tile_data_1_2[1, 2]
    assert retrieved_tiles[3][0][2, 2] == tile_data_2_2[2, 2]

    annotation_service.put_tiles(
        pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2, data=byte_data, compress=True
    )

    tile_response = annotation_service.get_tiles(
        pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2
    )

    retrieved_tiles = annotation_service.slice_bulk_data(
        pixelmap=pixelmap, tiles_data=tile_response, start_x=1, start_y=1, end_x=2, end_y=2
    )

    assert retrieved_tiles[0][0][1, 1] == tile_data_1_1[1, 1]
    assert retrieved_tiles[1][0][2, 1] == tile_data_2_1[2, 1]
    assert retrieved_tiles[2][0][1, 2] == tile_data_1_2[1, 2]
    assert retrieved_tiles[3][0][2, 2] == tile_data_2_2[2, 2]


def test_harpy_service():
    harpy_service = HarpyService()

    # Questionnaires

    questionnaire_post_response = harpy_service.post_questionaire()
    questionnaire_get_response = harpy_service.get_questionaire(questionnaire_id=questionnaire_post_response["id"])
    assert questionnaire_get_response == questionnaire_post_response

    # TODO: test PUT /fhir/questionnaires route

    questionnaire_query_response = harpy_service.put_questionaire_query(query={})
    assert questionnaire_query_response["item_count"] > 0
    assert len(questionnaire_query_response["items"]) > 0

    questionnaire_history_response = harpy_service.get_questionaire_history(questionnaire_get_response["id"])
    assert len(questionnaire_history_response) == 1
    qr_history_item_response = harpy_service.get_questionaire_history_item(
        questionnaire_id=questionnaire_get_response["id"],
        version_id=questionnaire_history_response[0]["meta"]["versionId"],
    )
    assert qr_history_item_response == questionnaire_history_response[0]

    # QuestionnaireResponses

    qr_post_response = harpy_service.post_questionaire_response()
    qr_get_response = harpy_service.get_questionaire_response(questionnaire_response_id=qr_post_response["id"])
    assert qr_get_response == qr_post_response

    # TODO: test PUT /fhir/questionnaire-responses route

    # TODO: does not work as expected -> return 422
    # qr_query_response = harpy_service.put_questionaire_response_query(query={})
    # assert qr_query_response["item_count"] > 0
    # assert len(qr_query_response["items"]) > 0

    qr_history_response = harpy_service.get_questionaire_response_history(
        questionnaire_response_id=qr_get_response["id"]
    )
    assert len(qr_history_response) == 1
    qr_history_item_response = harpy_service.get_questionaire_response_history_item(
        questionnaire_response_id=qr_get_response["id"], version_id=qr_history_response[0]["meta"]["versionId"]
    )
    assert qr_history_item_response == qr_history_response[0]

    # Selectors

    selector = {
        "type": "Questionnaire",
        "logical_id": questionnaire_get_response["id"],
        "selector_value": f"selector-{uuid.uuid4()}",
    }
    post_selector = harpy_service.post_selector(payload=selector)
    get_selector = harpy_service.get_selector(selector_id=post_selector["id"])
    assert post_selector == get_selector
    put_selector_query = harpy_service.put_selector_query(payload={})
    assert put_selector_query["item_count"] > 0
    assert len(put_selector_query["items"]) > 0

    selector_tagging = {
        "type": "Questionnaire",
        "selector_value": selector["selector_value"],
        "indication": f"indication-{uuid.uuid4()}",
        "procedure": f"procedure-{uuid.uuid4()}",
    }
    post_selector_tagging = harpy_service.post_selector_tagging(payload=selector_tagging)
    get_selector_tagging = harpy_service.get_selector_tagging(selector_tagging_id=post_selector_tagging["id"])
    assert post_selector_tagging == get_selector_tagging
    put_selector_tagging_query = harpy_service.get_selector_taggings()
    assert put_selector_tagging_query["item_count"] > 0
    assert len(put_selector_tagging_query["items"]) > 0

    _ = harpy_service.delete_selector(selector_id=get_selector["id"])
    _ = harpy_service.delete_selector_tagging(selector_tagging_id=get_selector_tagging["id"])
