import uuid

import requests

from .settings import Settings


class JobService:
    settings = Settings()

    def create_job(self, app_id, creator_type="USER", mode="STANDALONE", containerized=True):
        endpoint = "/v3/jobs"
        job_example = {
            "app_id": app_id,
            "creator_id": "not_defined",
            "creator_type": creator_type,
            "mode": mode,
            "containerized": containerized,
        }
        url = self.settings.mds_url + endpoint
        r = requests.post(url, json=job_example)

        assert r.status_code == 200
        return r.json()

    def update_job_input(self, job_id, input_id, input_key="my_wsi"):
        endpoint = f"/v3/jobs/{job_id}/inputs/{input_key}"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, json={"id": input_id})
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        assert isinstance(r.json(), dict)

    def update_job_selector_value(self, job_id, input_key, selector_value):
        endpoint = f"/v3/jobs/{job_id}/inputs/{input_key}/selector"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, json={"selector_value": selector_value})
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        assert isinstance(r.json(), dict)

    def get_job_selector_value(self, job_id, input_key):
        endpoint = f"/v3/jobs/{job_id}/inputs/{input_key}/selector"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        assert isinstance(r.json(), dict)
        return r.json()

    def delete_job_input(self, job_id):
        endpoint = f"/v3/jobs/{job_id}/inputs/my_wsi"
        url = self.settings.mds_url + endpoint
        r = requests.delete(url)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        assert isinstance(r.json(), dict)

    def update_job_output(self, job_id, output_key="my_result", slide_id=str(uuid.uuid4())):
        endpoint = f"/v3/jobs/{job_id}/outputs/my_wsi"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, json={"id": slide_id})
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200

    def delete_job_output(self, job_id):
        endpoint = f"/v3/jobs/{job_id}/outputs/my_wsi"
        url = self.settings.mds_url + endpoint
        r = requests.delete(url)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        assert isinstance(r.json(), dict)

    def read_job(self, job_id):
        endpoint = f"/v3/jobs/{job_id}"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        return r.json()

    def query_jobs(self, statuses=None):
        endpoint = "/v3/jobs/query"
        url = self.settings.mds_url + endpoint
        data = {"statuses": statuses}
        r = requests.put(url, json=data)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        return r.json()

    def query_jobs_by_creator_types(self, creator_types):
        endpoint = "/v3/jobs/query"
        url = self.settings.mds_url + endpoint
        data = {"creator_types": creator_types}
        r = requests.put(url, json=data)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        return r.json()

    def update_job_status(self, job_id, status):
        endpoint = f"/v3/jobs/{job_id}/status"
        url = self.settings.mds_url + endpoint
        data = {"status": status}
        r = requests.put(url, json=data)
        assert r.status_code == 200

    def update_job_progress(self, job_id, progress: float):
        endpoint = f"/v3/jobs/{job_id}/progress"
        url = self.settings.mds_url + endpoint
        r = requests.put(url, json={"progress": progress})
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200

    def update_validation_status(self, i_or_o, job_id, validation_status, error_message=None):
        endpoint = f"/v3/jobs/{job_id}/{i_or_o}-validation-status"
        url = self.settings.mds_url + endpoint
        data = {"validation_status": validation_status, "error_message": error_message}
        r = requests.put(url, json=data)
        assert r.status_code == 200

    def get_public_key(self):
        endpoint = "/v3/jobs/public-key"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()

    def delete_job(self, job_id):
        endpoint = f"/v3/jobs/{job_id}"
        url = self.settings.mds_url + endpoint
        r = requests.delete(url)
        assert r.status_code == 200

    def get_token(self, job_id):
        endpoint = f"/v3/jobs/{job_id}/token"
        url = self.settings.mds_url + endpoint
        r = requests.get(url)
        assert r.status_code == 200
        return r.json()
