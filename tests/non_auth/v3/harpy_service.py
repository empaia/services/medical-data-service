from tests.non_auth.v3.base_service import BaseService

from .settings import Settings

SAMPLE_QUESTIONNAIRE = {
    "resourceType": "Questionnaire",
    "name": "ASLPA1",
    "title": "ASLP.A1 Adult Sleep Studies",
    "status": "active",
    "description": "Adult Sleep Studies Prior Authorization Form",
    "useContext": [
        {
            "code": {
                "system": "http://terminology.hl7.org/CodeSystem/usage-context-type",
                "code": "task",
                "display": "Workflow Task",
            },
            "valueCodeableConcept": {
                "coding": [
                    {
                        "system": "http://fhir.org/guides/nachc/hiv-cds/CodeSystem/activity-codes",
                        "code": "ASLP.A1",
                        "display": "Adult Sleep Studies",
                    }
                ]
            },
        }
    ],
    "copyrightLabel": "Some Copyright label. All rights reserved.",
    "item": [
        {
            "linkId": "0",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-sleep-study-order",
            "text": "A sleep study procedure being ordered",
            "type": "group",
            "repeats": True,
            "item": [
                {
                    "linkId": "1",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.code",
                    "text": "A sleep study procedure being ordered",
                    "type": "choice",
                    "answerValueSet": "http://example.org/sdh/dtr/aslp/ValueSet/aslp-a1-de1-codes-grouper",
                },
                {
                    "linkId": "2",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.occurrence[x]",
                    "text": "Date of the procedure",
                    "type": "dateTime",
                },
            ],
        },
        {
            "linkId": "3",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-diagnosis-of-obstructive-sleep-apnea#Condition.code",
            "text": "Diagnosis of Obstructive Sleep Apnea",
            "type": "choice",
            "answerValueSet": "http://example.org/sdh/dtr/aslp/ValueSet/aslp-a1-de17",
        },
        {
            "linkId": "4",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-history-of-hypertension#Observation.value[x]",
            "text": "History of Hypertension",
            "type": "boolean",
        },
        {
            "linkId": "5",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-history-of-diabetes#Observation.value[x]",
            "text": "History of Diabetes",
            "type": "boolean",
        },
        {
            "linkId": "6",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-height#Observation.value[x]",
            "text": "Neck circumference (in inches)",
            "type": "quantity",
        },
        {
            "linkId": "7",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-height#Observation.value[x]",
            "text": "Height (in inches)",
            "type": "quantity",
        },
        {
            "linkId": "8",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-weight#Observation.value[x]",
            "text": "Weight (in pounds)",
            "type": "quantity",
        },
        {
            "linkId": "9",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-bmi#Observation.value[x]",
            "text": "Body mass index (BMI)",
            "type": "quantity",
        },
    ],
}


SAMPLE_QUESTIONNAIRE_RESPONSE = {
    "resourceType": "QuestionnaireResponse",
    "questionnaire": "Questionnaire/1",
    "status": "in-progress",
    "item": [
        {
            "linkId": "0",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-sleep-study-order",
            "text": "A sleep study procedure being ordered",
            "item": [
                {
                    "linkId": "1",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.code",
                    "text": "A sleep study procedure being ordered",
                    "answer": [
                        {
                            "valueCoding": {
                                "system": "http://example.org/sdh/dtr/aslp/CodeSystem/aslp-codes",
                                "code": "ASLP.A1.DE2",
                                "display": "Home sleep apnea testing (HSAT)",
                            }
                        }
                    ],
                },
                {
                    "linkId": "2",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.occurrence[x]",
                    "text": "Date of the procedure",
                    "answer": [{"valueDateTime": "2023-04-10T08:00:00.000Z"}],
                },
            ],
        },
        {
            "linkId": "0",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-sleep-study-order",
            "text": "A sleep study procedure being ordered",
            "item": [
                {
                    "linkId": "1",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.code",
                    "text": "A sleep study procedure being ordered",
                    "answer": [
                        {
                            "valueCoding": {
                                "system": "http://example.org/sdh/dtr/aslp/CodeSystem/aslp-codes",
                                "code": "ASLP.A1.DE14",
                                "display": "Artificial intelligence (AI)",
                            }
                        }
                    ],
                },
                {
                    "linkId": "2",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.occurrence[x]",
                    "text": "Date of the procedure",
                    "answer": [{"valueDateTime": "2023-04-15T08:00:00.000Z"}],
                },
            ],
        },
        {
            "linkId": "3",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-diagnosis-of-obstructive-sleep-apnea#Condition.code",
            "text": "Diagnosis of Obstructive Sleep Apnea",
            "answer": [
                {
                    "valueCoding": {
                        "system": "http://example.org/sdh/dtr/aslp/CodeSystem/aslp-codes",
                        "code": "ASLP.A1.DE17",
                        "display": "Obstructive sleep apnea (OSA)",
                    }
                }
            ],
        },
        {
            "linkId": "4",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-history-of-hypertension#Observation.value[x]",
            "text": "History of Hypertension",
            "answer": [{"valueBoolean": True}],
        },
        {
            "linkId": "5",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-history-of-diabetes#Observation.value[x]",
            "text": "History of Diabetes",
            "answer": [{"valueBoolean": True}],
        },
        {
            "linkId": "6",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-height#Observation.value[x]",
            "text": "Neck circumference (in inches)",
            "answer": [
                {
                    "valueQuantity": {
                        "value": 16,
                        "unit": "[in_i]",
                        "system": "http://unitsofmeasure.org",
                        "code": "[in_i]",
                    }
                }
            ],
        },
        {
            "linkId": "7",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-height#Observation.value[x]",
            "text": "Height (in inches)",
            "answer": [
                {
                    "valueQuantity": {
                        "value": 69,
                        "unit": "[in_i]",
                        "system": "http://unitsofmeasure.org",
                        "code": "[in_i]",
                    }
                }
            ],
        },
        {
            "linkId": "8",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-weight#Observation.value[x]",
            "text": "Weight (in pounds)",
            "answer": [
                {
                    "valueQuantity": {
                        "value": 185,
                        "unit": "[lb_av]",
                        "system": "http://unitsofmeasure.org",
                        "code": "[lb_av]",
                    }
                }
            ],
        },
        {
            "linkId": "9",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-bmi#Observation.value[x]",
            "text": "Body mass index (BMI)",
            "answer": [
                {
                    "valueQuantity": {
                        "value": 16.2,
                        "unit": "kg/m2",
                        "system": "http://unitsofmeasure.org",
                        "code": "kg/m2",
                    }
                }
            ],
        },
    ],
}


class HarpyService(BaseService):
    settings = Settings()

    # FHIR resources

    def post_questionaire(self, expected_status_code=201):
        return self._post(
            "/v3/fhir/questionnaires", expected_status_code=expected_status_code, json=SAMPLE_QUESTIONNAIRE
        )

    def put_questionaire(self, questionnaire_id: str, payload: dict, expected_status_code=201):
        return self._put(
            f"/v3/fhir/questionnaires/{questionnaire_id}", expected_status_code=expected_status_code, json=payload
        )

    def get_questionaire(self, questionnaire_id: str, expected_status_code=200):
        return self._get(f"/v3/fhir/questionnaires/{questionnaire_id}", expected_status_code=expected_status_code)

    def get_questionaire_history(self, questionnaire_id: str, expected_status_code=200):
        return self._get(
            f"/v3/fhir/questionnaires/{questionnaire_id}/history", expected_status_code=expected_status_code
        )

    def get_questionaire_history_item(self, questionnaire_id: str, version_id: str, expected_status_code=200):
        return self._get(
            f"/v3/fhir/questionnaires/{questionnaire_id}/history/{version_id}",
            expected_status_code=expected_status_code,
        )

    def put_questionaire_query(self, query: dict, expected_status_code=200):
        return self._put(f"/v3/fhir/questionnaires/query", expected_status_code=expected_status_code, json=query)

    def post_questionaire_response(self, expected_status_code=201):
        return self._post(
            "/v3/fhir/questionnaire-responses",
            expected_status_code=expected_status_code,
            json=SAMPLE_QUESTIONNAIRE_RESPONSE,
        )

    def put_questionaire_response(self, questionnaire_response_id: str, payload: dict, expected_status_code=201):
        return self._put(
            f"/v3/fhir/questionnaires/{questionnaire_response_id}",
            expected_status_code=expected_status_code,
            json=payload,
        )

    def get_questionaire_response(self, questionnaire_response_id: str, expected_status_code=200):
        return self._get(
            f"/v3/fhir/questionnaire-responses/{questionnaire_response_id}", expected_status_code=expected_status_code
        )

    def put_questionaire_response_query(self, query: dict, expected_status_code=200):
        return self._put(
            f"/v3/fhir/questionnaire-responses/query", expected_status_code=expected_status_code, json=query
        )

    def get_questionaire_response_history(self, questionnaire_response_id: str, expected_status_code=200):
        return self._get(
            f"/v3/fhir/questionnaire-responses/{questionnaire_response_id}/history",
            expected_status_code=expected_status_code,
        )

    def get_questionaire_response_history_item(
        self, questionnaire_response_id: str, version_id: str, expected_status_code=200
    ):
        return self._get(
            f"/v3/fhir/questionnaire-responses/{questionnaire_response_id}/history/{version_id}",
            expected_status_code=expected_status_code,
        )

    # Selectors

    def post_selector(self, payload: dict, expected_status_code=201):
        return self._post("/v3/fhir/selectors", expected_status_code=expected_status_code, json=payload)

    def put_selector_query(self, payload: dict, expected_status_code=200):
        return self._put("/v3/fhir/selectors/query", expected_status_code=expected_status_code, json=payload)

    def get_selector(self, selector_id: str, expected_status_code=200):
        return self._get(f"/v3/fhir/selectors/{selector_id}", expected_status_code=expected_status_code)

    def delete_selector(self, selector_id: str, expected_status_code=200):
        return self._delete(f"/v3/fhir/selectors/{selector_id}", expected_status_code=expected_status_code)

    def post_selector_tagging(self, payload: dict, expected_status_code=201):
        return self._post("/v3/fhir/selector-taggings", expected_status_code=expected_status_code, json=payload)

    def get_selector_taggings(self, expected_status_code=200):
        return self._get("/v3/fhir/selector-taggings", expected_status_code=expected_status_code)

    def get_selector_tagging(self, selector_tagging_id: str, expected_status_code=200):
        return self._get(f"/v3/fhir/selector-taggings/{selector_tagging_id}", expected_status_code=expected_status_code)

    def delete_selector_tagging(self, selector_tagging_id: str, expected_status_code=200):
        return self._delete(
            f"/v3/fhir/selector-taggings/{selector_tagging_id}", expected_status_code=expected_status_code
        )
