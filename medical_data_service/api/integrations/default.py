from fastapi import Depends, HTTPException, status


class Default:
    def __init__(self, settings, logger):
        self.settings = settings
        self.logger = logger

    def global_depends(self):
        return Depends(_unauthorized)

    async def case_hook(self, case_id, auth_payload):
        pass

    async def cases_hook(self, auth_payload):
        pass

    async def cases_filter_hook(self, case_query, auth_payload):
        pass

    async def slides_hook(self, auth_payload):
        pass

    async def slides_filter_hook(self, slide_query, auth_payload):
        pass

    async def examinations_hook(self, auth_payload):
        pass

    async def jobs_hook(self, auth_payload):
        pass

    async def questionnaires_hook(self, auth_payload):
        pass

    async def selectors_hook(self, auth_payload):
        pass

    async def questionnaire_responses_filter_hook(self, questonnaire_responses_query, auth_payload):
        pass


def _unauthorized():
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Integration not configured",
    )
