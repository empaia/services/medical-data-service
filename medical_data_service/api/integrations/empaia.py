from logging import Logger

from fastapi import Depends, Request
from fastapi.openapi.models import OAuthFlowClientCredentials, OAuthFlows
from fastapi.security import OAuth2
from pydantic import BaseModel
from pydantic_settings import BaseSettings, SettingsConfigDict

from ...empaia_receiver_auth import Auth


class AuthSettings(BaseSettings):
    idp_url: str
    audience: str
    refresh_interval: int = 300  # seconds
    rewrite_url_in_wellknown: str = ""
    openapi_token_url: str = ""

    model_config = SettingsConfigDict(env_file=".env", env_prefix="mds_", extra="ignore")


class Payload(BaseModel):
    decoded_token: dict
    request: dict


def make_oauth2_wrapper(auth: Auth, auth_settings: AuthSettings):
    oauth2_scheme = OAuth2(
        flows=OAuthFlows(clientCredentials=OAuthFlowClientCredentials(tokenUrl=auth_settings.openapi_token_url))
    )

    def oauth2_wrapper(request: Request, token=Depends(oauth2_scheme)):
        # checks audience (if no exception, user is allowed to use the service)
        decoded_token = auth.decode_token(token)
        return Payload(decoded_token=decoded_token, request=request)

    return oauth2_wrapper


class EmpaiaApiIntegration:
    def __init__(self, settings, logger: Logger):
        self.settings = settings
        self.logger = logger

        self.auth_settings = AuthSettings()

        self.auth = Auth(
            idp_url=self.auth_settings.idp_url.rstrip("/"),
            refresh_interval=self.auth_settings.refresh_interval,
            audience=self.auth_settings.audience,
            rewrite_url_in_wellknown=self.auth_settings.rewrite_url_in_wellknown,
            logger=self.logger,
        )
        self.oauth2_wrapper = make_oauth2_wrapper(auth=self.auth, auth_settings=self.auth_settings)
        self.logger.info("Loaded EmpaiaApiIntegration")

    def global_depends(self):
        return Depends(self.oauth2_wrapper)

    async def case_hook(self, case_id, auth_payload):
        pass

    async def cases_hook(self, auth_payload):
        pass

    async def cases_filter_hook(self, case_query, auth_payload):
        pass

    async def slides_hook(self, auth_payload):
        pass

    async def slides_filter_hook(self, slide_query, auth_payload):
        pass

    async def examinations_hook(self, auth_payload):
        pass

    async def jobs_hook(self, auth_payload):
        pass

    async def questionnaires_hook(self, auth_payload):
        pass

    async def selectors_hook(self, auth_payload):
        pass

    async def questionnaire_responses_filter_hook(self, questonnaire_responses_query, auth_payload):
        pass
