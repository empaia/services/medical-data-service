import aiohttp
from fastapi import status

from ... import __version__
from ...singletons import http_client, settings

services = [
    {"name": "clinical-data-service", "url": settings.cds_url},
    {"name": "annotation-service", "url": settings.as_url},
    {"name": "examination-service", "url": settings.es_url},
    {"name": "job-service", "url": settings.js_url},
]
services = [service for service in services if service["url"] is not None]


async def _get_service_alive(service):
    async with http_client.client as client:
        try:
            async with client.get(service["url"] + "/alive") as r:
                result = await r.json()
            return result
        except aiohttp.ClientConnectorError as e:
            return {"status": f"ERROR: {service['name']} not reachable!", "details": str(e)}


def _check_all_services_alive(services_response_dict):
    return all([r.get("status") == "ok" for r in services_response_dict.values()])


def add_routes_alive(app):
    @app.get("/alive", status_code=status.HTTP_200_OK, tags=["Private"])
    async def _():
        services_response_dict = {}
        for service in services:
            services_response_dict[service["name"]] = await _get_service_alive(service)

        response = {"services": services_response_dict, "version": __version__}
        if _check_all_services_alive(services_response_dict):
            response["status"] = "ok"
        else:
            unreachable_services_str = ", ".join(
                [r for r in services_response_dict if services_response_dict[r].get("status") != "ok"]
            )
            response["status"] = f"ERROR: {unreachable_services_str} not reachable!"
        return response
