from fastapi import status

from ...models.v1.storage import PutSlideStorage, SlideStorage
from ...singletons import api_integration, http_client, settings


def add_routes_v1_storage(app):
    base_url = settings.cds_url.rstrip("/")

    @app.put(
        "/slides/{slide_id}/storage", status_code=status.HTTP_200_OK, response_model=SlideStorage, tags=["Private"]
    )
    async def _(slide_id: str, slide_storage: PutSlideStorage, _=api_integration.global_depends()):
        return await http_client.put_stream_response(
            f"{base_url}/private/v1/slides/{slide_id}/storage", json=slide_storage.model_dump()
        )

    @app.get("/slides/{slide_id}/storage", response_model=SlideStorage, tags=["Private"])
    async def _(slide_id: str, _=api_integration.global_depends()):
        return await http_client.get_stream_response(f"{base_url}/private/v1/slides/{slide_id}/storage")

    @app.delete("/slides/{slide_id}/storage", tags=["Private"])
    async def _(slide_id: str, _=api_integration.global_depends()):
        return await http_client.delete_stream_response(f"{base_url}/private/v1/slides/{slide_id}/storage")
