from ...singletons import settings
from .v1_clinical import add_routes_v1_clinical
from .v1_misc import add_routes_v1_misc
from .v1_storage import add_routes_v1_storage


def add_routes_private_v1(app):
    if settings.enable_storage_routes:
        add_routes_v1_storage(app)

    add_routes_v1_clinical(app)
    add_routes_v1_misc(app)
