from fastapi import Body, Path, Query

from ...models.v1.clinical import Case, ClinicalSlide, PostCase, PostClinicalSlide, PutCase, PutClinicalSlide
from ...singletons import api_integration, http_client, settings
from ..v1.helpers import params


def add_routes_v1_clinical(app):
    base_url = settings.cds_url.rstrip("/")

    @app.post(
        "/cases",
        response_model=Case,
        tags=["Private"],
        responses={
            200: {"description": "The newly created Case."},
        },
    )
    async def _(
        case: PostCase = Body(..., description="The Case to be added to the DB"),
        external_ids=Query(
            False, description="Use the id provided in the body. Depending on the configuration this might be disabled"
        ),
        _=api_integration.global_depends(),
    ) -> str:
        """Create a new Case and add it to the Data Base."""
        return await http_client.post_stream_response(
            f"{base_url}/private/v1/cases", json=case.model_dump(), params=params(external_ids=external_ids)
        )

    @app.put(
        "/cases/{case_id}",
        response_model=Case,
        tags=["Private"],
        responses={
            200: {"description": "Updated Case"},
            400: {"description": "Illegal or read-only attribute"},
        },
    )
    async def _(
        case_id: str = Path(..., description="The ID of the case to update"),
        put_case: PutCase = Body(..., description="Dictionary with attributes to be updated"),
        _=api_integration.global_depends(),
    ) -> Case:
        """Update the Case with that ID with the given template, or raise Error if Case does not exist."""
        return await http_client.put_stream_response(
            f"{base_url}/private/v1/cases/{case_id}", json=put_case.model_dump()
        )

    @app.post(
        "/slides",
        response_model=ClinicalSlide,
        tags=["Private"],
        responses={
            200: {"description": "The newly created Slide"},
            400: {"description": "Case not found"},
        },
    )
    async def _(
        slide: PostClinicalSlide = Body(..., description="The Slide to be added to the Data Base"),
        external_ids=Query(
            False, description="Use the id provided in the body. Depending on the configuration this might be disabled"
        ),
        _=api_integration.global_depends(),
    ) -> str:
        """Create a new Slide description and add it to the Data Base. May raise Exception
        e.g. if the case ID or the given enum values for tissue or stain are invalid.
        """
        return await http_client.post_stream_response(
            f"{base_url}/private/v1/slides", json=slide.model_dump(), params=params(external_ids=external_ids)
        )

    @app.put(
        "/slides/{slide_id}",
        response_model=ClinicalSlide,
        tags=["Private"],
        responses={
            200: {"description": "Updated Slide"},
            400: {"description": "Illegal or read-only attribute"},
        },
    )
    async def _(
        slide_id: str = Path(..., description="The ID of the case to update"),
        put_slide: PutClinicalSlide = Body(..., description="Dictionary with attributes to be updated"),
        _=api_integration.global_depends(),
    ) -> ClinicalSlide:
        """Update the Slide with that ID with the given template, or raise Error if Slide does not exist."""
        return await http_client.put_stream_response(
            f"{base_url}/private/v1/slides/{slide_id}", json=put_slide.model_dump()
        )

    @app.get("/tags", response_model=dict, tags=["Private"])
    async def _(_=api_integration.global_depends()) -> dict:
        """Get Dictionary of allowed STAIN and TISSUE (organ) tags"""
        return await http_client.get_stream_response(f"{base_url}/v1/tags")
