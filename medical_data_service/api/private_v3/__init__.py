from ...singletons import settings
from .v3_clinical import add_routes_v3_clinical
from .v3_misc import add_routes_v3_misc
from .v3_storage import add_routes_v3_storage


def add_routes_private_v3(app):
    if settings.enable_storage_routes:
        add_routes_v3_storage(app)

    add_routes_v3_clinical(app)
    add_routes_v3_misc(app)
