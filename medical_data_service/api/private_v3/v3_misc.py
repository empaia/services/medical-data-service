from ...models.v3.annotation.server_settings import AnnotationServiceSettings
from ...singletons import api_integration, http_client, settings


def add_routes_v3_misc(app):
    base_url = settings.as_url.rstrip("/")

    @app.get(
        "/annotation-service/settings",
        tags=["Private"],
        responses={
            200: {"model": AnnotationServiceSettings},
        },
    )
    async def _(_=api_integration.global_depends()):
        return await http_client.get_stream_response(f"{base_url}/v3/settings")
