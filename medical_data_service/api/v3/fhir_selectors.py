from typing import Annotated

from pydantic import Field

from ...models.v3.fhir.selectors import (
    PostSelector,
    PostSelectorTagging,
    Selector,
    SelectorList,
    SelectorQuery,
    SelectorTagging,
    SelectorTaggingList,
)
from ...singletons import api_integration, http_client, settings


def add_routes_fhir_selectors(app):
    base_url = settings.hs_url.rstrip("/")

    @app.post(
        "/fhir/selectors",
        response_model=Selector,
        status_code=201,
        summary="Create a new selector item",
        tags=["FHIR Selectors"],
    )
    async def _(
        selector: PostSelector,
        payload=api_integration.global_depends(),
    ) -> Selector:
        await api_integration.selectors_hook(auth_payload=payload)
        return await http_client.post_stream_response(f"{base_url}/v3/fhir/selectors", json=selector.model_dump())

    @app.put(
        "/fhir/selectors/query",
        response_model=SelectorList,
        summary="Query selectors",
        tags=["FHIR Selectors"],
    )
    async def _(
        query: SelectorQuery,
        skip: Annotated[int, Field(ge=0)] = None,
        limit: Annotated[int, Field(ge=0)] = None,
        payload=api_integration.global_depends(),
    ) -> SelectorList:
        await api_integration.selectors_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/fhir/selectors/query", json=query.model_dump(), params={"skip": skip, "limit": limit}
        )

    @app.get(
        "/fhir/selectors/{selector_id}",
        response_model=Selector,
        summary="Get selector item by ID",
        tags=["FHIR Selectors"],
    )
    async def _(
        selector_id: str,
        payload=api_integration.global_depends(),
    ) -> Selector:
        await api_integration.selectors_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/fhir/selectors/{selector_id}")

    @app.delete(
        "/fhir/selectors/{selector_id}",
        summary="Delete a selector item by ID",
        response_model=Selector,
        tags=["FHIR Selectors"],
    )
    async def _(
        selector_id: str,
        payload=api_integration.global_depends(),
    ) -> Selector:
        await api_integration.selectors_hook(auth_payload=payload)
        return await http_client.delete_stream_response(f"{base_url}/v3/fhir/selectors/{selector_id}")

    @app.post(
        "/fhir/selector-taggings",
        response_model=SelectorTagging,
        status_code=201,
        summary="Create a new selector tagging item",
        tags=["FHIR Selectors"],
    )
    async def _(
        selector_tagging: PostSelectorTagging,
        payload=api_integration.global_depends(),
    ) -> SelectorTagging:
        await api_integration.selectors_hook(auth_payload=payload)
        return await http_client.post_stream_response(
            f"{base_url}/v3/fhir/selector-taggings", json=selector_tagging.model_dump()
        )

    @app.get(
        "/fhir/selector-taggings",
        response_model=SelectorTaggingList,
        summary="Get all selector tagging items",
        tags=["FHIR Selectors"],
    )
    async def _(
        skip: Annotated[int, Field(ge=0)] = None,
        limit: Annotated[int, Field(ge=0)] = None,
        payload=api_integration.global_depends(),
    ) -> SelectorTaggingList:
        await api_integration.selectors_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/fhir/selector-taggings")

    @app.get(
        "/fhir/selector-taggings/{selector_tagging_id}",
        response_model=SelectorTagging,
        summary="Get selector tagging item by ID",
        tags=["FHIR Selectors"],
    )
    async def _(
        selector_tagging_id: str,
        payload=api_integration.global_depends(),
    ) -> SelectorTagging:
        await api_integration.selectors_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/fhir/selector-taggings/{selector_tagging_id}")

    @app.delete(
        "/fhir/selector-taggings/{selector_tagging_id}",
        summary="Delete a selector tagging item by ID",
        response_model=SelectorTagging,
        tags=["FHIR Selectors"],
    )
    async def _(
        selector_tagging_id: str,
        payload=api_integration.global_depends(),
    ) -> SelectorTagging:
        await api_integration.selectors_hook(auth_payload=payload)
        return await http_client.delete_stream_response(f"{base_url}/v3/fhir/selector-taggings/{selector_tagging_id}")
