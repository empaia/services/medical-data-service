from fastapi import Body, Path, Query
from pydantic import conint

from ...models.v3.commons import IdObject, ItemCount
from ...models.v3.job import (
    Job,
    JobList,
    JobQuery,
    JobToken,
    PostJob,
    PutJobProgress,
    PutJobStatus,
    PutJobValidationStatus,
    PutSelectorValue,
    SelectorValue,
)
from ...singletons import api_integration, http_client, settings


def add_routes_job(app):
    base_url = settings.js_url.rstrip("/")

    @app.post(
        "/jobs",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "The full newly created Job"},
        },
    )
    async def _(
        req: PostJob = Body(..., description="Core attributes of the Job to be created"),
        payload=api_integration.global_depends(),
    ) -> Job:
        """Checks job request, e.g. whether the app exists, derives full Job object and stores it in database."""
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.post_stream_response(f"{base_url}/v3/jobs", json=req.model_dump())

    @app.get(
        "/jobs/public-key",
        response_model=bytes,
        tags=["Job"],
        responses={
            200: {"description": "Public key for validating Access Tokens created for Jobs"},
        },
    )
    async def _(payload=api_integration.global_depends()) -> bytes:
        """Get the public key that can be used for validating the Access Tokens created for the Jobs."""
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/public-key")

    @app.get(
        "/jobs/{job_id}/token",
        response_model=JobToken,
        tags=["Job"],
        responses={
            200: {"description": "The newly created Access Token"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to create the Token for"),
        expire: conint(gt=0) = Query(86400, description="Valid time in seconds (default: valid for 24h)"),
        payload=api_integration.global_depends(),
    ) -> JobToken:
        """Create and return an Access-Token that is needed by the Job-Execution-Service to run the Job."""
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/jobs/{job_id}/token", params={"expire": expire})

    @app.get(
        "/jobs/{job_id}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "The corresponding Job, with or without embedded EAD"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to retrieve"),
        payload=api_integration.global_depends(),
    ) -> Job:
        """Get Job from database and return it, with all details and current status,
        or raise exception if it does not exist. Optionally include full EAD.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/jobs/{job_id}")

    @app.delete(
        "/jobs/{job_id}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "The removed Job"},
            400: {"description": "Job not found or invalid state"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to remove"),
        payload=api_integration.global_depends(),
    ) -> Job:
        """Delete Job from database, if it exists and still in state ASSEMBLY, otherwise raise error.
        The deleted Job is returned by the service.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.delete_stream_response(f"{base_url}/v3/jobs/{job_id}")

    @app.get(
        "/jobs",
        response_model=JobList,
        tags=["Job"],
        responses={
            200: {"description": "List of Jobs (or sublist, with paging)"},
        },
    )
    async def _(
        skip: ItemCount = Query(None, description="Number of items to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
        payload=api_integration.global_depends(),
    ) -> JobList:
        """Get list of all Jobs, optionally with skip and limit parameters for paging.
        Jobs are returned sorted by created_at field, descending order, newest first.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/jobs", params={"skip": skip, "limit": limit})

    @app.put(
        "/jobs/query",
        response_model=JobList,
        tags=["Job"],
        responses={
            200: {"description": "List of matching items, including total number available"},
        },
    )
    async def _(
        query: JobQuery = Body(..., description="Query for filtering the returned items"),
        skip: ItemCount = Query(None, description="Number of items to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
        payload=api_integration.global_depends(),
    ) -> JobList:
        """Get list of filtered Jobs, with paging."""
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/query", json=query.model_dump(), params={"skip": skip, "limit": limit}
        )

    @app.put(
        "/jobs/{job_id}/status",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object after Status update"},
            400: {"description": "Job not found or invalid status transition"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job whose status to update"),
        status: PutJobStatus = Body(..., description="New status and optional error message"),
        payload=api_integration.global_depends(),
    ) -> Job:
        """Get job from DB and update status; may be called by the app-service (for app-triggered completion) or WBS.
        Raise Exception if job not found or status invalid or invalid status transition.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.put_stream_response(f"{base_url}/v3/jobs/{job_id}/status", json=status.model_dump())

    @app.put(
        "/jobs/{job_id}/inputs/{input_key}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object with added input"},
            400: {"description": "Unknown input or input already set"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to add an input to"),
        input_key: str = Path(..., description="The name of the input to set"),
        input_id: IdObject = Body(..., description="Wrapper for the actual MDS Id of the input"),
        payload=api_integration.global_depends(),
    ) -> Job:
        """The job is created with no inputs. Use this to add inputs to the Job after creation, but before
        executing the Job. Set Job Status to `READY` when all inputs have been set (not done automatically).
        Raises Exception if job not found or input key does not match.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/inputs/{input_key}", json=input_id.model_dump()
        )

    @app.delete(
        "/jobs/{job_id}/inputs/{input_key}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object with deleted input"},
            400: {"description": "Unknown input id"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to delete an input from"),
        input_key: str = Path(..., description="The name of the input to delete"),
        payload=api_integration.global_depends(),
    ) -> Job:
        """Use this to delete already set inputs from the Job. Only works, if Job Status is `ASSEMBLY`.
        Raises Exception if job not found or input key does not exist or Status is not `ASSEMBLY`.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.delete_stream_response(f"{base_url}/v3/jobs/{job_id}/inputs/{input_key}")

    @app.put(
        "/jobs/{job_id}/outputs/{output_key}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object with added output"},
            400: {"description": "Unknown output or output already set"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to add an output to"),
        output_key: str = Path(..., description="The name of the output to set"),
        output_id: IdObject = Body(..., description="Wrapper for the actual MDS Id of the output"),
        payload=api_integration.global_depends(),
    ) -> Job:
        """To be called by app-service to add output-references to the Job in the database.
        Raises Exception if job not found or output key does not match.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/outputs/{output_key}", json=output_id.model_dump()
        )

    @app.delete(
        "/jobs/{job_id}/outputs/{output_key}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object with deleted output"},
            400: {"description": "Unknown output id"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to delete an output from"),
        output_key: str = Path(..., description="The name of the output to delete"),
        payload=api_integration.global_depends(),
    ) -> Job:
        """Use this to delete already set outputs for the Job. Only works, if Job Mode is `PREPROCESSING` or `REPORT`.
        Raises Exception if job not found or output key does not exist or Status is not `ASSEMBLY`.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.delete_stream_response(f"{base_url}/v3/jobs/{job_id}/outputs/{output_key}")

    # #############
    # # New in v3 #
    # #############

    @app.put(
        "/jobs/{job_id}/progress",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object after progress update"},
            400: {"description": "Job not found or invalid progress"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job whose progress to update"),
        progress: PutJobProgress = Body(..., description="Object with new progress value"),
        payload=api_integration.global_depends(),
    ) -> Job:
        """Get job from DB and update progress; may be called by the app-service.
        Raise Exception if job not found or invalid progress value.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/progress", json=progress.model_dump()
        )

    @app.put(
        "/jobs/{job_id}/input-validation-status",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object after input validation status update"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The id of the job whose input validation status to update"),
        validation_status_update: PutJobValidationStatus = Body(
            ...,
            description="New validation status and optional error message",
        ),
        payload=api_integration.global_depends(),
    ) -> Job:
        """Updates job input validation status; may be called by the workbench-daemon during job validation.
        Raise Exception if job not found or status invalid.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/input-validation-status", json=validation_status_update.model_dump()
        )

    @app.put(
        "/jobs/{job_id}/output-validation-status",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object after output validation status update"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The id of the job whose output validation status to update"),
        validation_status_update: PutJobValidationStatus = Body(
            ...,
            description="New validation status and optional error message",
        ),
        payload=api_integration.global_depends(),
    ) -> Job:
        """Updates job output validation status; may be called by the workbench-daemon during job validation.
        Raise Exception if job not found or status invalid.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/output-validation-status", json=validation_status_update.model_dump()
        )

    @app.put(
        "/jobs/{job_id}/inputs/{input_key}/selector",
        response_model=SelectorValue,
        tags=["Job"],
        responses={
            200: {"description": "Job object after Selector Value for input key update"},
            400: {"description": "Job not found or input key not set"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job whose Selector Value to update"),
        input_key: str = Path(..., description="The name of the input to set"),
        selector_value: PutSelectorValue = Body(
            ..., description="Unique string used to identify a FHIR Resource in the EMPAIA namespace format"
        ),
        payload=api_integration.global_depends(),
    ) -> SelectorValue:
        """Get Job with Job Id from DB and set Selector Value and Input key in separate table.
        Raise Exception if Job Status is not in Assembly or the input key is not set.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/inputs/{input_key}/selector", json=selector_value.model_dump()
        )

    @app.get(
        "/jobs/{job_id}/inputs/{input_key}/selector",
        response_model=SelectorValue,
        tags=["Job"],
        responses={
            200: {"description": "The Selector Value of the specified Job Id and input key"},
            404: {"description": "Job Id or input key not found"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Selector Value to retrieve"),
        input_key: str = Path(..., description="The input key of the Selector Value to retrieve"),
        payload=api_integration.global_depends(),
    ) -> SelectorValue:
        """Get Selector Value of Job with the corresponding Job Id and input key from DB and return it,
        or raise exception if either one of the parameters does not exist.
        """
        await api_integration.jobs_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/jobs/{job_id}/inputs/{input_key}/selector")
