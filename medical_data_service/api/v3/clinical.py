from typing import Annotated

from fastapi import Body, Header, Path, Query

from ...models.v3.clinical import Case, CaseList, CaseQuery, ClinicalSlide, ClinicalSlideList, ClinicalSlideQuery
from ...models.v3.commons import ItemCount
from ...singletons import api_integration, http_client, settings


def add_routes_clinical(app):
    base_url = settings.cds_url.rstrip("/")

    @app.get(
        "/cases/{case_id}",
        response_model=Case,
        tags=["Clinical"],
        responses={
            200: {"description": "The full Case (except Slides) if it exists"},
            400: {"description": "Case not found"},
        },
    )
    async def _(
        case_id: str = Path(..., description="The ID of the case to retrieve"),
        with_slides=Query(False, description="Whether to include all slides in the returned case"),
        payload=api_integration.global_depends(),
    ) -> Case:
        """Get case with the given ID, if it exists. Slides are not included in the Case;
        to get all Slides for a specific case, use the /slide/query route.
        """
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/cases/{case_id}", params={"with_slides": with_slides}
        )

    @app.get(
        "/cases",
        response_model=CaseList,
        tags=["Clinical"],
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
    )
    async def _(
        skip: ItemCount = Query(None, description="Number of cases to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
        with_slides=Query(False, description="Whether to include all slides in the returned cases"),
        payload=api_integration.global_depends(),
    ) -> CaseList:
        """Get list of Cases, with paging."""
        await api_integration.cases_hook(auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/cases", params={"skip": skip, "limit": limit, "with_slides": with_slides}
        )

    @app.put(
        "/cases/query",
        response_model=CaseList,
        tags=["Clinical"],
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
    )
    async def _(
        case_query: CaseQuery,
        skip: ItemCount = Query(None, description="Number of cases to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
        with_slides=Query(False, description="Whether to include all slides in the returned cases"),
        payload=api_integration.global_depends(),
    ) -> CaseList:
        """Get list of filtered Cases, with paging."""
        await api_integration.cases_filter_hook(case_query=case_query, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/cases/query",
            json=case_query.model_dump(),
            params={"skip": skip, "limit": limit, "with_slides": with_slides},
        )

    @app.get(
        "/slides/{slide_id}",
        response_model=ClinicalSlide,
        tags=["Clinical"],
        responses={
            200: {"description": "The full Slide, if it exists"},
            400: {"description": "Slide not found"},
        },
    )
    async def _(
        slide_id: str = Path(..., description="The ID of the Slide to retrieve"),
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ) -> ClinicalSlide:
        """Get the Slide with the given ID, if it exists."""
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/slides/{slide_id}",
            headers={"case-id": case_id},
        )

    @app.get(
        "/slides",
        response_model=ClinicalSlideList,
        tags=["Clinical"],
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
    )
    async def _(
        skip: ItemCount = Query(None, description="Number of slides to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
        payload=api_integration.global_depends(),
    ) -> ClinicalSlideList:
        """Get list of all Slides, with paging."""
        await api_integration.slides_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/slides", params={"skip": skip, "limit": limit})

    @app.put(
        "/slides/query",
        response_model=ClinicalSlideList,
        tags=["Clinical"],
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
    )
    async def _(
        slide_query: ClinicalSlideQuery = Body(..., description="Query for filtering the returned items"),
        skip: ItemCount = Query(None, description="Number of cases to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
        payload=api_integration.global_depends(),
    ) -> ClinicalSlideList:
        """Get list of filtered Slides, with paging."""
        await api_integration.slides_filter_hook(slide_query=slide_query, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/slides/query", json=slide_query.model_dump(), params={"skip": skip, "limit": limit}
        )
