from ...models.v3.commons import Id, Message
from ...models.v3.examination import (
    AppUiStorage,
    Examination,
    ExaminationList,
    ExaminationQuery,
    PostExamination,
    PostPreprocessingRequest,
    PostPreprocessingTrigger,
    PostScope,
    PreprocessingRequest,
    PreprocessingRequestList,
    PreprocessingRequestQuery,
    PreprocessingTrigger,
    PreprocessingTriggerList,
    PutExaminationState,
    PutPreprocessingRequestUpdate,
    Scope,
    ScopeList,
    ScopeQuery,
    ScopeToken,
)
from ...singletons import api_integration, http_client, settings


def add_routes_examination(app):
    base_url = settings.es_url.rstrip("/")

    @app.get(
        "/examinations",
        responses={
            200: {"model": ExaminationList},
            500: {"model": Message, "description": "No examinations could be retrieved"},
        },
        tags=["Examination"],
    )
    async def _(
        skip: int = None,
        limit: int = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/examinations", params={"skip": skip, "limit": limit}
        )

    @app.put(
        "/examinations",
        responses={
            200: {"model": Examination, "description": "Existing open examination"},
            201: {"model": Examination, "description": "Created open examination"},
            500: {"model": Message, "description": "Examination could not be created"},
        },
        tags=["Examination"],
    )
    async def _(
        post_examination: PostExamination,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.put_stream_response(f"{base_url}/v3/examinations", json=post_examination.model_dump())

    @app.put(
        "/examinations/query",
        responses={
            200: {"model": ExaminationList},
            500: {"model": Message, "description": "No examinations could be retrieved"},
        },
        tags=["Examination"],
    )
    async def _(
        query: ExaminationQuery,
        skip: int = None,
        limit: int = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/examinations/query", json=query.model_dump(), params={"skip": skip, "limit": limit}
        )

    @app.get(
        "/examinations/public-key",
        response_model=bytes,
        responses={
            200: {"description": "Public key for validating Access Tokens created for Jobs"},
        },
        tags=["Examination"],
    )
    async def _(payload=api_integration.global_depends()) -> bytes:
        """Get the public key that can be used for validating the Access Tokens created for the Jobs."""
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/public-key")

    @app.get(
        "/examinations/{ex_id}",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        ex_id: Id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/examinations/{ex_id}")

    @app.put(
        "/examinations/{ex_id}/state",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            409: {
                "model": Message,
                "description": "Only one open examination allowed per case / app pair",
            },
        },
        tags=["Examination"],
    )
    async def _(
        ex_id: Id,
        state_update: PutExaminationState,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/examinations/{ex_id}/state", json=state_update.model_dump()
        )

    @app.put(
        "/examinations/{ex_id}/jobs/{job_id}/add",
        responses={
            200: {"model": Examination, "description": "If job was already in examination."},
            201: {"model": Examination, "description": "If job was newly added to examination."},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {
                "model": Message,
                "description": "The resource is CLOSED",
            },
        },
        tags=["Examination"],
    )
    async def _(
        ex_id: Id,
        job_id: Id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.put_stream_response(f"{base_url}/v3/examinations/{ex_id}/jobs/{job_id}/add")

    @app.delete(
        "/examinations/{ex_id}/jobs/{job_id}",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {
                "model": Message,
                "description": "The resource is CLOSED",
            },
        },
        tags=["Examination"],
    )
    async def _(
        ex_id: Id,
        job_id: Id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.delete_stream_response(f"{base_url}/v3/examinations/{ex_id}/jobs/{job_id}")

    @app.put(
        "/scopes",
        responses={
            200: {"model": Scope, "description": "Scope already exists. Returning existing."},
            201: {"model": Scope, "description": "Scope created."},
            404: {
                "model": Message,
                "description": "Could not create or get scope. No examination with given id exists.",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope: PostScope,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.put_stream_response(f"{base_url}/v3/scopes/", json=scope.model_dump())

    @app.put(
        "/scopes/query",
        responses={
            200: {"model": ScopeList},
            500: {"model": Message, "description": "No scopes could be retrieved"},
        },
        tags=["Examination"],
    )
    async def _(
        query: ScopeQuery,
        skip: int = None,
        limit: int = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/scopes/query", json=query.model_dump(), params={"skip": skip, "limit": limit}
        )

    @app.get(
        "/scopes/{scope_id}",
        responses={
            200: {"model": Scope},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope_id: Id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/scopes/{scope_id}")

    @app.get(
        "/scopes/{scope_id}/token",
        responses={
            200: {"model": ScopeToken},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope_id: Id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/scopes/{scope_id}/token")

    # #############
    # # New in v3 #
    # #############

    @app.post(
        "/preprocessing-triggers",
        status_code=201,
        responses={
            201: {"model": PreprocessingTrigger},
            409: {
                "model": Message,
                "description": "The trigger combination already exists",
            },
        },
        tags=["Examination"],
    )
    async def _(
        trigger: PostPreprocessingTrigger,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.post_stream_response(
            f"{base_url}/v3/preprocessing-triggers", json=trigger.model_dump()
        )

    @app.get(
        "/preprocessing-triggers",
        responses={
            200: {"model": PreprocessingTriggerList},
        },
        tags=["Examination"],
    )
    async def _(
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/preprocessing-triggers")

    @app.get(
        "/preprocessing-triggers/{trigger_id}",
        responses={
            200: {"model": PreprocessingTrigger},
            404: {"model": Message, "description": "The trigger does not exist"},
        },
        tags=["Examination"],
    )
    async def _(
        trigger_id: Id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/preprocessing-triggers/{trigger_id}")

    @app.delete(
        "/preprocessing-triggers/{trigger_id}",
        status_code=204,
        responses={
            204: {"model": None},
            404: {"model": Message, "description": "The trigger does not exist"},
        },
        tags=["Examination"],
    )
    async def _(
        trigger_id: Id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.delete_stream_response(f"{base_url}/v3/preprocessing-triggers/{trigger_id}")

    @app.post(
        "/preprocessing-requests",
        status_code=201,
        responses={
            201: {"model": PreprocessingRequest},
        },
        tags=["Examination"],
    )
    async def _(
        request: PostPreprocessingRequest,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.post_stream_response(
            f"{base_url}/v3/preprocessing-requests", json=request.model_dump()
        )

    @app.get(
        "/preprocessing-requests/{request_id}",
        responses={
            200: {"model": PreprocessingRequest},
            404: {"model": Message, "description": "The request does not exist"},
        },
        tags=["Examination"],
    )
    async def _(
        request_id: Id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/preprocessing-requests/{request_id}")

    @app.put(
        "/preprocessing-requests/query",
        status_code=200,
        responses={
            200: {"model": PreprocessingRequestList},
        },
        tags=["Examination"],
    )
    async def _(
        query: PreprocessingRequestQuery,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/preprocessing-requests/query", json=query.model_dump()
        )

    @app.put(
        "/preprocessing-requests/{request_id}/process",
        status_code=200,
        responses={
            200: {"model": PreprocessingRequest},
            404: {"model": Message, "description": "The request does not exist"},
            409: {"model": Message, "description": "The request has already been processed"},
        },
        tags=["Examination"],
    )
    async def _(
        request_id: Id,
        update: PutPreprocessingRequestUpdate,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/preprocessing-requests/{request_id}/process", json=update.model_dump()
        )

    @app.get(
        "/scopes/{scope_id}/app-ui-storage/user",
        responses={
            200: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope_id: Id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/scopes/{scope_id}/app-ui-storage/user",
        )

    @app.get(
        "/scopes/{scope_id}/app-ui-storage/scope",
        responses={
            200: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope_id: Id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/scopes/{scope_id}/app-ui-storage/scope",
        )

    @app.put(
        "/scopes/{scope_id}/app-ui-storage/user",
        status_code=201,
        responses={
            201: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope_id: Id,
        storage: AppUiStorage,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/scopes/{scope_id}/app-ui-storage/user",
            json=storage.model_dump(),
        )

    @app.put(
        "/scopes/{scope_id}/app-ui-storage/scope",
        status_code=201,
        responses={
            201: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope_id: Id,
        storage: AppUiStorage,
        payload=api_integration.global_depends(),
    ):
        await api_integration.examinations_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/scopes/{scope_id}/app-ui-storage/scope",
            json=storage.model_dump(),
        )
