from typing import Annotated, List

from fastapi import Header, Request
from pydantic import Field

from ...models.v3.fhir.questionnaire_responses import (
    PostQuestionnaireResponse,
    QuestionnaireResponse,
    QuestionnaireResponseList,
    QuestionnaireResponseQuery,
)
from ...models.v3.fhir.questionnaires import PostQuestionnaire, Questionnaire, QuestionnaireList, QuestionnaireQuery
from ...singletons import api_integration, http_client, settings


def add_routes_fhir_resources(app):
    base_url = settings.hs_url.rstrip("/")

    @app.post(
        "/fhir/questionnaires",
        response_model=Questionnaire,
        status_code=201,
        summary="Create a questionnaire",
        tags=["FHIR Resources"],
    )
    async def _(
        request: Request,
        questionnaire: PostQuestionnaire,
        payload=api_integration.global_depends(),
    ) -> Questionnaire:
        await api_integration.questionnaires_hook(auth_payload=payload)
        return await http_client.post_stream_response(
            f"{base_url}/v3/fhir/questionnaires", data=request.stream(), headers={"Content-Type": "application/json"}
        )

    @app.put(
        "/fhir/questionnaires/query",
        response_model=QuestionnaireList,
        summary="Query all questionnaires",
        tags=["FHIR Resources"],
    )
    async def _(
        query: QuestionnaireQuery,
        skip: Annotated[int, Field(ge=0)] = None,
        limit: Annotated[int, Field(ge=0)] = None,
        payload=api_integration.global_depends(),
    ) -> QuestionnaireList:
        await api_integration.questionnaires_hook(auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/fhir/questionnaires/query", json=query.model_dump(), params={"skip": skip, "limit": limit}
        )

    @app.put(
        "/fhir/questionnaires/{logical_id}",
        response_model=Questionnaire,
        summary="Update a questionnaire",
        tags=["FHIR Resources"],
    )
    async def _(
        logical_id: str,
        questionnaire: PostQuestionnaire,
        payload=api_integration.global_depends(),
    ) -> Questionnaire:
        await api_integration.questionnaires_hook(auth_payload=payload)
        data = questionnaire.model_dump(by_alias=True, exclude_unset=True, exclude_none=True, mode="json")
        return await http_client.put_stream_response(f"{base_url}/v3/fhir/questionnaires/{logical_id}", json=data)

    @app.get(
        "/fhir/questionnaires/{logical_id}",
        response_model=Questionnaire,
        summary="Get a questionnaire item by logical ID",
        tags=["FHIR Resources"],
    )
    async def _(
        logical_id: str,
        payload=api_integration.global_depends(),
    ) -> Questionnaire:
        await api_integration.questionnaires_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/fhir/questionnaires/{logical_id}")

    @app.get(
        "/fhir/questionnaires/{logical_id}/history",
        response_model=List[Questionnaire],
        summary="Retrieve all history items of a questionnaire",
        tags=["FHIR Resources"],
    )
    async def _(
        logical_id: str,
        payload=api_integration.global_depends(),
    ) -> List[Questionnaire]:
        await api_integration.questionnaires_hook(auth_payload=payload)
        return await http_client.get_stream_response(f"{base_url}/v3/fhir/questionnaires/{logical_id}/history")

    @app.get(
        "/fhir/questionnaires/{logical_id}/history/{version_id}",
        response_model=Questionnaire,
        summary="Get a certain questionnaire history item by ID and version",
        tags=["FHIR Resources"],
    )
    async def _(
        logical_id: str,
        version_id: str,
        payload=api_integration.global_depends(),
    ) -> Questionnaire:
        await api_integration.questionnaires_hook(auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/fhir/questionnaires/{logical_id}/history/{version_id}"
        )

    @app.post(
        "/fhir/questionnaire-responses",
        response_model=QuestionnaireResponse,
        status_code=201,
        summary="Create a new questionnaire response",
        tags=["FHIR Resources"],
    )
    async def _(
        request: Request,
        questionnaire_response: PostQuestionnaireResponse,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ) -> QuestionnaireResponse:
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.post_stream_response(
            f"{base_url}/v3/fhir/questionnaire-responses",
            data=request.stream(),
            headers={"Content-Type": "application/json", "case-id": case_id},
        )

    @app.put(
        "/fhir/questionnaire-responses/query",
        response_model=QuestionnaireResponseList,
        summary="Query questionnaire responses",
        tags=["FHIR Resources"],
    )
    async def _(
        query: QuestionnaireResponseQuery,
        skip: Annotated[int, Field(ge=0)] = None,
        limit: Annotated[int, Field(ge=0)] = None,
        payload=api_integration.global_depends(),
    ) -> QuestionnaireResponseList:
        await api_integration.questionnaire_responses_filter_hook(
            questonnaire_responses_query=query, auth_payload=payload
        )
        return await http_client.put_stream_response(
            f"{base_url}/v3/fhir/questionnaire-responses/query",
            json=query.model_dump(),
            params={"skip": skip, "limit": limit},
        )

    @app.put(
        "/fhir/questionnaire-responses/{logical_id}",
        response_model=QuestionnaireResponse,
        summary="Update a questionnaire response",
        tags=["FHIR Resources"],
    )
    async def _(
        logical_id: str,
        questionnaire_response: PostQuestionnaireResponse,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ) -> QuestionnaireResponse:
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        data = questionnaire_response.model_dump(by_alias=True, exclude_unset=True, exclude_none=True, mode="json")
        return await http_client.put_stream_response(
            f"{base_url}/v3/fhir/questionnaire-responses/{logical_id}", json=data, headers={"case-id": case_id}
        )

    @app.get(
        "/fhir/questionnaire-responses/{logical_id}",
        response_model=QuestionnaireResponse,
        summary="Get a questionnaire response by ID",
        tags=["FHIR Resources"],
    )
    async def _(
        logical_id: str,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ) -> QuestionnaireResponse:
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/fhir/questionnaire-responses/{logical_id}", headers={"case-id": case_id}
        )

    @app.get(
        "/fhir/questionnaire-responses/{logical_id}/history",
        response_model=List[QuestionnaireResponse],
        summary="Get all history items of a questionnaire response",
        tags=["FHIR Resources"],
    )
    async def _(
        logical_id: str,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ) -> List[QuestionnaireResponse]:
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/fhir/questionnaire-responses/{logical_id}/history", headers={"case-id": case_id}
        )

    @app.get(
        "/fhir/questionnaire-responses/{logical_id}/history/{version_id}",
        response_model=QuestionnaireResponse,
        summary="Get a certain questionnaire response history item by ID and version",
        tags=["FHIR Resources"],
    )
    async def _(
        logical_id: str,
        version_id: str,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ) -> QuestionnaireResponse:
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/fhir/questionnaire-responses/{logical_id}/history/{version_id}",
            headers={"case-id": case_id},
        )
