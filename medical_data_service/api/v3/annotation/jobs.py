from typing import Annotated

from fastapi import Header

from ....models.v3.annotation.jobs import JobLock
from ....models.v3.commons import Id, Message
from ....singletons import api_integration, http_client, settings


def add_routes_jobs(app):
    base_url = settings.as_url.rstrip("/")

    @app.put(
        "/jobs/{job_id}/lock/annotations/{annotation_id}",
        tags=["Annotation"],
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        job_id: Id,
        annotation_id: Id,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/lock/annotations/{annotation_id}",
            headers={"case-id": case_id},
        )

    @app.put(
        "/jobs/{job_id}/lock/classes/{class_id}",
        tags=["Annotation"],
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        job_id: Id,
        class_id: Id,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/lock/classes/{class_id}",
            headers={"case-id": case_id},
        )

    @app.put(
        "/jobs/{job_id}/lock/collections/{collection_id}",
        tags=["Annotation"],
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        job_id: Id,
        collection_id: Id,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/lock/collections/{collection_id}",
            headers={"case-id": case_id},
        )

    @app.put(
        "/jobs/{job_id}/lock/primitives/{primitive_id}",
        tags=["Annotation"],
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        job_id: Id,
        primitive_id: Id,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/lock/primitives/{primitive_id}",
            headers={"case-id": case_id},
        )

    @app.put(
        "/jobs/{job_id}/lock/slides/{slide_id}",
        tags=["Annotation"],
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        job_id: Id,
        slide_id: Id,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/lock/slides/{slide_id}",
            headers={"case-id": case_id},
        )

    @app.put(
        "/jobs/{job_id}/lock/pixelmaps/{pixelmap_id}",
        tags=["Annotation"],
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        job_id: Id,
        pixelmap_id: Id,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/jobs/{job_id}/lock/pixelmaps/{pixelmap_id}",
            headers={"case-id": case_id},
        )
