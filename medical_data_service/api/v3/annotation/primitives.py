from typing import Annotated

from fastapi import Header, Request

from ....models.v3.annotation.primitives import (
    PostPrimitive,
    PostPrimitives,
    Primitive,
    PrimitiveList,
    PrimitiveQuery,
    Primitives,
)
from ....models.v3.commons import Id, IdObject, Message, UniqueReferences
from ....singletons import api_integration, http_client, settings


def add_routes_primitives(app):
    base_url = settings.as_url.rstrip("/")

    @app.get(
        "/primitives",
        tags=["Annotation"],
        responses={
            200: {"model": PrimitiveList},
        },
    )
    async def _(
        skip: int = None,
        limit: int = None,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/primitives",
            params={"skip": skip, "limit": limit},
            headers={"case-id": case_id},
        )

    @app.post(
        "/primitives",
        tags=["Annotation"],
        status_code=201,
        responses={201: {"model": Primitives}},
    )
    async def _(
        primitives: PostPrimitives,
        request: Request,
        external_ids: bool = False,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.post_stream_response(
            f"{base_url}/v3/primitives",
            data=request.stream(),
            params={"external_ids": external_ids},
            headers={"case-id": case_id, "Content-Type": "application/json"},
        )

    @app.put(
        "/primitives/query",
        tags=["Annotation"],
        status_code=200,
        responses={200: {"model": PrimitiveList}},
    )
    async def _(
        primitives_query: PrimitiveQuery,
        skip: int = None,
        limit: int = None,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/primitives/query",
            json=primitives_query.model_dump(),
            params={"skip": skip, "limit": limit},
            headers={"case-id": case_id},
        )

    @app.put(
        "/primitives/query/unique-references",
        tags=["Annotation"],
        status_code=200,
        responses={
            200: {"model": UniqueReferences},
        },
    )
    async def _(
        primitives_query: PrimitiveQuery,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/primitives/query/unique-references",
            json=primitives_query.model_dump(),
            headers={"case-id": case_id},
        )

    @app.get(
        "/primitives/{primitive_id}",
        tags=["Annotation"],
        responses={
            200: {"model": Primitive},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        primitive_id: Id, case_id: Annotated[str | None, Header()] = None, payload=api_integration.global_depends()
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/primitives/{primitive_id}",
            headers={"case-id": case_id},
        )

    @app.put(
        "/primitives/{primitive_id}",
        tags=["Annotation"],
        responses={
            200: {"model": Primitive},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Primitive is locked"},
        },
    )
    async def _(
        primitive_id: Id,
        primitive: PostPrimitive,
        request: Request,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/v3/primitives/{primitive_id}",
            data=request.stream(),
            headers={"case-id": case_id, "Content-Type": "application/json"},
        )

    @app.delete(
        "/primitives/{primitive_id}",
        tags=["Annotation"],
        responses={
            200: {"model": IdObject},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Primitive is locked"},
        },
    )
    async def _(
        primitive_id: Id, case_id: Annotated[str | None, Header()] = None, payload=api_integration.global_depends()
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.delete_stream_response(
            f"{base_url}/v3/primitives/{primitive_id}",
            headers={"case-id": case_id},
        )
