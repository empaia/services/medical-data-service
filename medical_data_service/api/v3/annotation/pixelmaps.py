from typing import Annotated

from fastapi import Header, Path, Request
from fastapi.responses import StreamingResponse

from ....models.v3.annotation.pixelmaps import (
    Pixelmap,
    PixelmapList,
    PixelmapQuery,
    Pixelmaps,
    PixelmapSlideInfo,
    PostPixelmaps,
    UpdatePixelmap,
)
from ....models.v3.commons import Id, IdObject, Message, UniqueReferences
from ....singletons import api_integration, http_client, settings

PixelmapTileResponse = {
    200: {
        "content": {
            "application/octet-stream": {"schema": {"type": "string", "format": "binary"}},
        },
        "description": "Pixelmap tile response.",
    },
    404: {"model": Message, "description": "Pixelmap not found"},
}


def add_routes_pixelmaps(app):
    base_url = settings.as_url.rstrip("/") + "/v3/pixelmaps"

    @app.post(
        "/pixelmaps",
        tags=["Annotation"],
        summary="Creates a new pixelmap",
        status_code=201,
        responses={201: {"model": Pixelmaps}},
    )
    async def _(
        pixelmaps: PostPixelmaps,
        request: Request,
        external_ids: bool = False,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.post_stream_response(
            f"{base_url}",
            data=request.stream(),
            params={"external_ids": external_ids},
            headers={"case-id": case_id, "Content-Type": "application/json"},
        )

    @app.put(
        "/pixelmaps/query",
        tags=["Annotation"],
        summary="Returns a list of filtered pixelmaps",
        status_code=200,
        responses={200: {"model": PixelmapList}},
    )
    async def _(
        pixelmap_query: PixelmapQuery,
        skip: int = None,
        limit: int = None,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/query",
            json=pixelmap_query.model_dump(),
            params={"skip": skip, "limit": limit},
            headers={"case-id": case_id},
        )

    @app.put(
        "/pixelmaps/query/unique-references",
        tags=["Annotation"],
        summary="Returns a list of unique references for pixelmaps matching filter criteria",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        pixelmap_query: PixelmapQuery,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/query/unique-references",
            json=pixelmap_query.model_dump(),
            headers={"case-id": case_id},
        )

    @app.get(
        "/pixelmaps/{pixelmap_id}",
        tags=["Annotation"],
        summary="Returns a Pixelmap",
        status_code=200,
        responses={200: {"model": Pixelmap}, 404: {"model": Message, "description": "Pixelmap not found"}},
    )
    async def _(
        pixelmap_id: Id, case_id: Annotated[str | None, Header()] = None, payload=api_integration.global_depends()
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/{pixelmap_id}",
            headers={"case-id": case_id},
        )

    @app.put(
        "/pixelmaps/{pixelmap_id}",
        tags=["Annotation"],
        summary="Update a Pixelmap",
        status_code=200,
        responses={
            200: {"model": Pixelmap},
            404: {"model": Message, "description": "Pixelmap not found"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        pixelmap_id: Id,
        update_pixelmap: UpdatePixelmap,
        request: Request,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/{pixelmap_id}",
            data=request.stream(),
            headers={"case-id": case_id, "Content-Type": "application/json"},
        )

    @app.delete(
        "/pixelmaps/{pixelmap_id}",
        tags=["Annotation"],
        summary="Deletes the pixelmap",
        status_code=200,
        responses={
            200: {"model": IdObject},
            404: {"model": Message, "description": "Pixelmap not found"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        pixelmap_id: Id, case_id: Annotated[str | None, Header()] = None, payload=api_integration.global_depends()
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.delete_stream_response(
            f"{base_url}/{pixelmap_id}",
            headers={"case-id": case_id},
        )

    @app.put(
        "/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        tags=["Annotation"],
        summary="Uploads data for a tile of a pixelmap",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            422: {"model": Message, "description": "Invalid parameters"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        pixelmap_id: Id,
        request: Request,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        tile_x: int = Path(example=0, description="Request the tile_x-th tile in x dimension"),
        tile_y: int = Path(example=0, description="Request the tile_y-th tile in y dimension"),
        content_encoding: Annotated[str | None, Header()] = None,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
            data=request.stream(),
            headers={
                "case-id": case_id,
                "Content-Encoding": content_encoding,
                "Content-Type": "application/octet-stream",
            },
        )

    @app.put(
        "/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
        tags=["Annotation"],
        summary="Bulk tile upload for a pixelmap",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            413: {"model": Message, "description": "Requested data too large"},
            422: {"model": Message, "description": "Invalid parameters"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        pixelmap_id: Id,
        request: Request,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        start_x: int = Path(example=0, description="Start position in x dimension"),
        start_y: int = Path(example=0, description="Start position in y dimension"),
        end_x: int = Path(example=0, description="End position in x dimension"),
        end_y: int = Path(example=0, description="End position in y dimension"),
        content_encoding: Annotated[str | None, Header()] = None,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.put_stream_response(
            f"{base_url}/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
            data=request.stream(),
            headers={
                "case-id": case_id,
                "Content-Encoding": content_encoding,
                "Content-Type": "application/octet-stream",
            },
        )

    @app.get(
        "/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        tags=["Annotation"],
        summary="Get the data for a tile of a pixelmap",
        status_code=200,
        responses=PixelmapTileResponse,
        response_class=StreamingResponse,
    )
    async def _(
        pixelmap_id: Id,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        tile_x: int = Path(example=0, description="Request the tile_x-th tile in x dimension"),
        tile_y: int = Path(example=0, description="Request the tile_y-th tile in y dimension"),
        accept_encoding: Annotated[str | None, Header()] = None,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)

        response = await http_client.get_stream_response(
            f"{base_url}/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
            headers={"case-id": case_id, "Accept-Encoding": accept_encoding},
        )
        return response

    @app.get(
        "/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
        tags=["Annotation"],
        summary="Bulk tile retrieval for a pixelmap",
        status_code=200,
        responses={
            200: {
                "content": {
                    "application/octet-stream": {"schema": {"type": "string", "format": "binary"}},
                },
                "description": "Pixelmap tile response.",
            },
            404: {"model": Message, "description": "Pixelmap not found"},
            413: {"model": Message, "description": "Requested data too large"},
            422: {"model": Message, "description": "Invalid parameters"},
        },
    )
    async def _(
        pixelmap_id: Id,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        start_x: int = Path(example=0, description="Start position in x dimension"),
        start_y: int = Path(example=0, description="Start position in y dimension"),
        end_x: int = Path(example=0, description="End position in x dimension"),
        end_y: int = Path(example=0, description="End position in y dimension"),
        accept_encoding: Annotated[str | None, Header()] = None,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
            headers={"case-id": case_id, "Accept-Encoding": accept_encoding},
        )

    @app.delete(
        "/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        tags=["Annotation"],
        summary="Delete the data for a tile of a pixelmap",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        pixelmap_id: Id,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        tile_x: int = Path(example=0, description="Request the tile_x-th tile in x dimension"),
        tile_y: int = Path(example=0, description="Request the tile_y-th tile in y dimension"),
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.delete_stream_response(
            f"{base_url}/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
            headers={"case-id": case_id},
        )

    @app.get(
        "/pixelmaps/{pixelmap_id}/info",
        tags=["Annotation"],
        summary="Get level and resolution of the referenced slide",
        status_code=200,
        responses={
            200: {"model": PixelmapSlideInfo},
            404: {"model": Message, "description": "Pixelmap not found or info not available"},
        },
    )
    async def _(
        pixelmap_id: Id,
        payload=api_integration.global_depends(),
        case_id: Annotated[str | None, Header()] = None,
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/{pixelmap_id}/info",
            headers={"case-id": case_id},
        )
