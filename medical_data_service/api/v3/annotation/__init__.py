from .annotations import add_routes_annotations
from .classes import add_routes_classes
from .collections import add_routes_collections
from .jobs import add_routes_jobs
from .pixelmaps import add_routes_pixelmaps
from .primitives import add_routes_primitives


def add_routes_annotation(app):
    add_routes_annotations(app)
    add_routes_classes(app)
    add_routes_collections(app)
    add_routes_jobs(app)
    add_routes_primitives(app)
    add_routes_pixelmaps(app)
