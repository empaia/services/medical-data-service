from ...singletons import settings
from .annotation import add_routes_annotation
from .clinical import add_routes_clinical
from .examination import add_routes_examination
from .fhir_resources import add_routes_fhir_resources
from .fhir_selectors import add_routes_fhir_selectors
from .job import add_routes_job
from .slide import add_routes_slide


def add_routes_v3(app):
    if settings.cds_url is not None:
        add_routes_clinical(app)
        add_routes_slide(app)
    if settings.es_url is not None:
        add_routes_examination(app)
    if settings.js_url is not None:
        add_routes_job(app)
    if settings.as_url is not None:
        add_routes_annotation(app)
    if settings.hs_url is not None:
        add_routes_fhir_resources(app)
        add_routes_fhir_selectors(app)
