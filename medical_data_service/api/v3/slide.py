from typing import Annotated, List

from fastapi import Header, Path, Query
from fastapi.responses import StreamingResponse

from ...models.v3.slide import SlideInfo
from ...singletons import api_integration, http_client, settings

ImageFormatsQuery = Query(
    "jpeg", description="Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff."
)

ImageQualityQuery = Query(
    90,
    ge=0,
    le=100,
    description=(
        "Image quality (Only for specific formats. For Jpeg files compression is always lossy. "
        "For tiff files 'deflate' compression is used by default. Set to 0 to compress lossy with 'jpeg')"
    ),
)

ImageChannelQuery = Query(None, description="List of requested image channels. By default all channels are returned.")

ImagePaddingColorQuery = Query(
    None,
    example="#FFFFFF",
    description=(
        "Background color as 24bit-hex-string with leading #, that is used when image tile contains "
        "whitespace when out of image extent. Default is white."
    ),
)

ZStackQuery = Query(0, description="Z-Stack layer index z")

ImageResponses = {200: {"content": {"image/*": {}}}}

ImageRegionResponse = ImageResponses
ImageRegionResponse[413] = {"detail": "Requested region is too large"}

DownloadResponses = {200: {"content": {"application/zip": {}}}}


def add_routes_slide(app):
    base_url = settings.cds_url.rstrip("/")

    @app.get("/slides/{slide_id}/info", response_model=SlideInfo, tags=["Slide"])
    async def _(
        slide_id: str, case_id: Annotated[str | None, Header()] = None, payload=api_integration.global_depends()
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/slides/{slide_id}/info",
            headers={"case-id": case_id},
        )

    @app.get("/slides/{slide_id}/download", responses=ImageResponses, response_class=StreamingResponse, tags=["Slide"])
    async def _(
        slide_id: str, case_id: Annotated[str | None, Header()] = None, payload=api_integration.global_depends()
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        stream = http_client.get_stream(
            url=f"{base_url}/v3/slides/{slide_id}/download",
            chunk_size=1_000_000,
            headers={"case-id": case_id},
        )
        rmd = await stream.__anext__()
        return http_client.StreamingResponse(
            content=stream,
            headers={
                "Content-Disposition": f"attachment;filename={slide_id}.zip",
            },
            status_code=rmd.status_code,
            media_type=rmd.media_type,
        )

    @app.get(
        "/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}",
        responses=ImageResponses,
        response_class=StreamingResponse,
        tags=["Slide"],
    )
    async def _(
        slide_id: str,
        max_x: int = Path(example=100, description="Maximum width of thumbnail"),
        max_y: int = Path(example=100, description="Maximum height of thumbnail"),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}",
            params={"image_format": image_format, "image_quality": image_quality},
            headers={"case-id": case_id},
        )

    @app.get(
        "/slides/{slide_id}/label/max_size/{max_x}/{max_y}",
        responses=ImageResponses,
        response_class=StreamingResponse,
        tags=["Slide"],
    )
    async def _(
        slide_id: str,
        max_x: int = Path(example=100, description="Maximum width of label image"),
        max_y: int = Path(example=100, description="Maximum height of label image"),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/slides/{slide_id}/label/max_size/{max_x}/{max_y}",
            params={"image_format": image_format, "image_quality": image_quality},
            headers={"case-id": case_id},
        )

    @app.get(
        "/slides/{slide_id}/macro/max_size/{max_x}/{max_y}",
        responses=ImageResponses,
        response_class=StreamingResponse,
        tags=["Slide"],
    )
    async def _(
        slide_id: str,
        max_x: int = Path(example=100, description="Maximum width of macro image"),
        max_y: int = Path(example=100, description="Maximum height of macro image"),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/slides/{slide_id}/macro/max_size/{max_x}/{max_y}",
            params={"image_format": image_format, "image_quality": image_quality},
            headers={"case-id": case_id},
        )

    @app.get(
        "/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
        responses=ImageRegionResponse,
        response_class=StreamingResponse,
        tags=["Slide"],
    )
    async def _(
        slide_id: str,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        start_x: int = Path(example=0, description="x component of start coordinate of requested region"),
        start_y: int = Path(example=0, description="y component of start coordinate of requested region"),
        size_x: int = Path(example=1024, description="Width of requested region"),
        size_y: int = Path(example=1024, description="Height of requested region"),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
        image_channels: List[int] = ImageChannelQuery,
        z: int = ZStackQuery,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
            params={
                "image_format": image_format,
                "image_quality": image_quality,
                "image_channels": image_channels,
                "z": z,
            },
            headers={"case-id": case_id},
        )

    @app.get(
        "/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}",
        responses=ImageResponses,
        response_class=StreamingResponse,
        tags=["Slide"],
    )
    async def _(
        slide_id: str,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        tile_x: int = Path(example=0, description="Request the tile_x-th tile in x dimension"),
        tile_y: int = Path(example=0, description="Request the tile_y-th tile in y dimension"),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
        image_channels: List[int] = ImageChannelQuery,
        padding_color: str = ImagePaddingColorQuery,
        z: int = ZStackQuery,
        case_id: Annotated[str | None, Header()] = None,
        payload=api_integration.global_depends(),
    ):
        await api_integration.case_hook(case_id=case_id, auth_payload=payload)
        return await http_client.get_stream_response(
            f"{base_url}/v3/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}",
            params={
                "image_format": image_format,
                "image_quality": image_quality,
                "image_channels": image_channels,
                "padding_color": padding_color,
                "z": z,
            },
            headers={"case-id": case_id},
        )
