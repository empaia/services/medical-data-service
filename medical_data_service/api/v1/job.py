from fastapi import Body, Path, Query
from pydantic import conint

from ...models.v1.commons import IdObject, ItemCount
from ...models.v1.job import Job, JobList, JobQuery, JobToken, PostJob, PutJobStatus
from ...singletons import api_integration, http_client, settings
from .helpers import params


def add_routes_job(app):
    base_url = settings.js_url.rstrip("/")

    @app.put(
        "/apps/{app_id}/ead",
        tags=["Job"],
        responses={
            200: {"description": "EAD successfully stored"},
            400: {"description": "App already exists or can not be stored due to other error"},
        },
    )
    async def _(
        app_id: str = Path(..., description="The Id of the App corresponding to the EAD"),
        ead: dict = Body(..., description="The full EAD in accordance with the JSON Schema"),
        _=api_integration.global_depends(),
    ):
        """Store EAD in database so it can be used in Jobs later."""
        return await http_client.put_stream_response(f"{base_url}/v1/apps/{app_id}/ead", json=ead)

    @app.get(
        "/apps/{app_id}/ead",
        response_model=dict,
        tags=["Job"],
        responses={
            200: {"description": "JSON dictionary representing the EAD corresponding to the App Id"},
            400: {"description": "EAD not found or other problem when retrieving the EAD"},
        },
    )
    async def _(
        app_id: str = Path(..., description="The Id of the App corresponding to the EAD"),
        _=api_integration.global_depends(),
    ) -> dict:
        """Get EAD for given App-ID from database."""
        return await http_client.get_stream_response(f"{base_url}/v1/apps/{app_id}/ead")

    @app.post(
        "/jobs",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "The full newly created Job"},
            400: {"description": "App/EAD not found"},
        },
    )
    async def _(
        req: PostJob = Body(..., description="Core attributes of the Job to be created"),
        _=api_integration.global_depends(),
    ) -> Job:
        """Checks job request, e.g. whether the app exists, derives full Job object and stores it in database."""
        return await http_client.post_stream_response(f"{base_url}/v1/jobs", json=req.model_dump())

    @app.get(
        "/jobs/{job_id}/token",
        response_model=JobToken,
        tags=["Job"],
        responses={
            200: {"description": "The newly created Access Token"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to create the Token for"),
        expire: conint(gt=0) = Query(86400, description="Valid time in seconds (default: valid for 24h)"),
        _=api_integration.global_depends(),
    ) -> JobToken:
        """Create and return an Access-Token that is needed by the Job-Execution-Service to run the Job."""
        return await http_client.get_stream_response(f"{base_url}/v1/jobs/{job_id}/token", params=params(expire=expire))

    @app.get(
        "/jobs/{job_id}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "The corresponding Job, with or without embedded EAD"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to retrieve"),
        with_ead: bool = Query(False, description="Whether to include the full EAD"),
        _=api_integration.global_depends(),
    ) -> Job:
        """Get Job from database and return it, with all details and current status,
        or raise exception if it does not exist. Optionally include full EAD.
        """
        return await http_client.get_stream_response(f"{base_url}/v1/jobs/{job_id}", params=params(with_ead=with_ead))

    @app.delete(
        "/jobs/{job_id}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "The removed Job"},
            400: {"description": "Job not found or invalid state"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to remove"),
        _=api_integration.global_depends(),
    ) -> Job:
        """Delete Job from database, if it exists and still in state ASSEMBLY, otherwise raise error.
        The deleted Job is returned by the service.
        """
        return await http_client.delete_stream_response(f"{base_url}/v1/jobs/{job_id}")

    @app.get(
        "/jobs",
        response_model=JobList,
        tags=["Job"],
        responses={
            200: {"description": "List of Jobs (or sublist, with paging)"},
        },
    )
    async def _(
        skip: ItemCount = Query(None, description="Number of items to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
        _=api_integration.global_depends(),
    ) -> JobList:
        """Get list of all Jobs, optionally with skip and limit parameters for paging.
        Jobs are returned sorted by created_at field, descending order, newest first.
        """
        return await http_client.get_stream_response(f"{base_url}/v1/jobs", params=params(skip=skip, limit=limit))

    @app.put(
        "/jobs/query",
        response_model=JobList,
        tags=["Job"],
        responses={
            200: {"description": "List of matching items, including total number available"},
        },
    )
    async def _(
        query: JobQuery = Body(..., description="Query for filtering the returned items"),
        skip: ItemCount = Query(None, description="Number of items to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
        _=api_integration.global_depends(),
    ) -> JobList:
        """Get list of filtered Jobs, with paging."""
        return await http_client.put_stream_response(
            f"{base_url}/v1/jobs/query", json=query.model_dump(), params=params(skip=skip, limit=limit)
        )

    @app.put(
        "/jobs/{job_id}/status",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object after Status update"},
            400: {"description": "Job not found or invalid status transition"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job whose status to update"),
        status: PutJobStatus = Body(..., description="New status and optional error message"),
        _=api_integration.global_depends(),
    ) -> Job:
        """Get job from DB and update status; may be called by the app-service (for app-triggered completion) or WBS.
        Raise Exception if job not found or status invalid or invalid status transition.
        """
        return await http_client.put_stream_response(f"{base_url}/v1/jobs/{job_id}/status", json=status.model_dump())

    @app.put(
        "/jobs/{job_id}/inputs/{input_key}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object with added input"},
            400: {"description": "Unknown input or input already set"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to add an input to"),
        input_key: str = Path(..., description="The name of the input to set"),
        input_id: IdObject = Body(..., description="Wrapper for the actual MDS Id of the input"),
        _=api_integration.global_depends(),
    ) -> Job:
        """The job is created with no inputs. Use this to add inputs to the Job after creation, but before
        executing the Job. Set Job Status to `READY` when all inputs have been set (not done automatically).
        Raises Exception if job not found or input key does not match.
        """
        return await http_client.put_stream_response(
            f"{base_url}/v1/jobs/{job_id}/inputs/{input_key}", json=input_id.model_dump()
        )

    @app.delete(
        "/jobs/{job_id}/inputs/{input_key}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object with deleted input"},
            400: {"description": "Unknown input id"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to delete an input from"),
        input_key: str = Path(..., description="The name of the input to delete"),
        _=api_integration.global_depends(),
    ) -> Job:
        """Use this to delete already set inputs from the Job. Only works, if Job Status is `ASSEMBLY`.
        Raises Exception if job not found or input key does not exist or Status is not `ASSEMBLY`.
        """
        return await http_client.delete_stream_response(f"{base_url}/v1/jobs/{job_id}/inputs/{input_key}")

    @app.put(
        "/jobs/{job_id}/outputs/{output_key}",
        response_model=Job,
        tags=["Job"],
        responses={
            200: {"description": "Job object with added output"},
            400: {"description": "Unknown output or output already set"},
        },
    )
    async def _(
        job_id: str = Path(..., description="The Id of the Job to add an output to"),
        output_key: str = Path(..., description="The name of the output to set"),
        output_id: IdObject = Body(..., description="Wrapper for the actual MDS Id of the output"),
        _=api_integration.global_depends(),
    ) -> Job:
        """To be called by app-service to add output-references to the Job in the database.
        Raises Exception if job not found or output key does not match.
        """
        return await http_client.put_stream_response(
            f"{base_url}/v1/jobs/{job_id}/outputs/{output_key}", json=output_id.model_dump()
        )

    @app.get(
        "/job-service/public-key",
        response_model=bytes,
        tags=["Job"],
        responses={
            200: {"description": "Public key for validating Access Tokens created for Jobs"},
        },
    )
    async def _(_=api_integration.global_depends()) -> bytes:
        """Get the public key that can be used for validating the Access Tokens created for the Jobs."""
        return await http_client.get_stream_response(f"{base_url}/v1/public-key")
