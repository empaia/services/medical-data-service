from ...singletons import settings
from .annotation import add_routes_annotation
from .clinical import add_routes_clinical
from .examination import add_routes_examination
from .job import add_routes_job
from .slide import add_routes_slide


def add_routes_v1(app):
    if settings.cds_url is not None:
        add_routes_clinical(app)
        add_routes_slide(app)
    if settings.es_url is not None:
        add_routes_examination(app)
    if settings.js_url is not None:
        add_routes_job(app)
    if settings.as_url is not None:
        add_routes_annotation(app)
