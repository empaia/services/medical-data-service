from fastapi import Request

from ....models.v1.annotation.annotations import (
    Annotation,
    AnnotationCountResponse,
    AnnotationList,
    AnnotationQuery,
    AnnotationQueryPosition,
    Annotations,
    AnnotationUniqueClassesQuery,
    AnnotationViewerList,
    AnnotationViewerQuery,
    PostAnnotations,
    UniqueClassValues,
)
from ....models.v1.commons import Id, IdObject, Message, UniqueReferences
from ....singletons import api_integration, http_client, settings
from ..helpers import params


def add_routes_annotations(app):
    base_url = settings.as_url.rstrip("/")

    @app.get(
        "/annotations",
        tags=["Annotation"],
        response_model=AnnotationList,
        responses={
            400: {"model": Message, "description": "Viewport not valid"},
        },
    )
    async def _(
        with_classes: bool = False,
        skip: int = None,
        limit: int = None,
        _=api_integration.global_depends(),
    ):
        return await http_client.get_stream_response(
            f"{base_url}/v1/annotations",
            params=params(with_classes=with_classes, skip=skip, limit=limit),
        )

    @app.post(
        "/annotations",
        tags=["Annotation"],
        status_code=201,
        responses={201: {"model": Annotations}},
    )
    async def _(
        annotations: PostAnnotations, request: Request, external_ids: bool = False, _=api_integration.global_depends()
    ):
        return await http_client.post_stream_response(
            f"{base_url}/v1/annotations",
            data=request.stream(),
            params=params(external_ids=external_ids),
            headers={"Content-Type": "application/json"},
        )

    @app.put(
        "/annotations/query",
        tags=["Annotation"],
        status_code=200,
        responses={
            200: {"model": AnnotationList},
            400: {
                "model": Message,
                "description": "npp_viewing must be specified when query parameter with_low_npp_centroids is true",
            },
        },
    )
    async def _(
        annotations_query: AnnotationQuery,
        with_classes: bool = False,
        with_low_npp_centroids: bool = False,
        skip: int = None,
        limit: int = None,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/annotations/query",
            json=annotations_query.model_dump(),
            params=params(
                with_classes=with_classes,
                with_low_npp_centroids=with_low_npp_centroids,
                skip=skip,
                limit=limit,
            ),
        )

    @app.put(
        "/annotations/query/count",
        tags=["Annotation"],
        status_code=200,
        responses={
            200: {"model": AnnotationCountResponse},
        },
    )
    async def _(
        annotations_query: AnnotationQuery,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/annotations/query/count",
            json=annotations_query.model_dump(),
        )

    @app.put(
        "/annotations/query/unique-class-values",
        tags=["Annotation"],
        status_code=200,
        responses={
            200: {"model": UniqueClassValues},
        },
    )
    async def _(
        annotations_query: AnnotationUniqueClassesQuery,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/annotations/query/unique-class-values",
            json=annotations_query.model_dump(),
        )

    @app.put(
        "/annotations/query/unique-references",
        tags=["Annotation"],
        status_code=200,
        responses={
            200: {"model": UniqueReferences},
        },
    )
    async def _(
        annotations_query: AnnotationQuery,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/annotations/query/unique-references",
            json=annotations_query.model_dump(),
        )

    @app.put(
        "/annotations/query/viewer",
        tags=["Annotation"],
        status_code=200,
        responses={
            200: {"model": AnnotationViewerList},
            400: {
                "model": Message,
                "description": "npp_viewing must be specified when query parameter with_low_npp_centroids is true",
            },
        },
    )
    async def _(
        annotations_query: AnnotationViewerQuery,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/annotations/query/viewer",
            json=annotations_query.model_dump(),
        )

    @app.get(
        "/annotations/{annotation_id}",
        tags=["Annotation"],
        responses={
            200: {"model": Annotation},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        annotation_id: Id,
        with_classes: bool = False,
        _=api_integration.global_depends(),
    ):
        return await http_client.get_stream_response(
            f"{base_url}/v1/annotations/{annotation_id}", params=params(with_classes=with_classes)
        )

    @app.put(
        "/annotations/{annotation_id}/query",
        tags=["Annotation"],
        responses={
            200: {"model": AnnotationQueryPosition},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        annotation_id: Id,
        annotations_query: AnnotationQuery,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/annotations/{annotation_id}/query", json=annotations_query.model_dump()
        )

    @app.delete(
        "/annotations/{annotation_id}",
        tags=["Annotation"],
        responses={
            200: {"model": IdObject},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Annotation is locked"},
        },
    )
    async def _(annotation_id: Id, _=api_integration.global_depends()):
        return await http_client.delete_stream_response(f"{base_url}/v1/annotations/{annotation_id}")
