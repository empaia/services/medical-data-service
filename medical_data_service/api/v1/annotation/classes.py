from fastapi import Request

from ....models.v1.annotation.classes import Class, Classes, ClassList, ClassQuery, PostClass, PostClasses
from ....models.v1.commons import Id, IdObject, Message, UniqueReferences
from ....singletons import api_integration, http_client, settings
from ..helpers import params


def add_routes_classes(app):
    base_url = settings.as_url.rstrip("/")

    @app.get(
        "/classes",
        tags=["Annotation"],
        responses={
            200: {"model": ClassList},
        },
    )
    async def _(
        with_unique_class_values: bool = False,
        skip: int = None,
        limit: int = None,
        _=api_integration.global_depends(),
    ):
        return await http_client.get_stream_response(
            f"{base_url}/v1/classes",
            params=params(with_unique_class_values=with_unique_class_values, skip=skip, limit=limit),
        )

    @app.post(
        "/classes",
        tags=["Annotation"],
        status_code=201,
        responses={201: {"model": Classes}},
    )
    async def _(classes: PostClasses, request: Request, external_ids: bool = False, _=api_integration.global_depends()):
        return await http_client.post_stream_response(
            f"{base_url}/v1/classes",
            data=request.stream(),
            params=params(external_ids=external_ids),
            headers={"Content-Type": "application/json"},
        )

    @app.put(
        "/classes/query",
        tags=["Annotation"],
        status_code=200,
        responses={200: {"model": ClassList}},
    )
    async def _(
        classes_query: ClassQuery,
        with_unique_class_values: bool = False,
        skip: int = None,
        limit: int = None,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/classes/query",
            json=classes_query.model_dump(),
            params=params(with_unique_class_values=with_unique_class_values, skip=skip, limit=limit),
        )

    @app.put(
        "/classes/query/unique-references",
        tags=["Annotation"],
        status_code=200,
        responses={
            200: {"model": UniqueReferences},
        },
    )
    async def _(
        classes_query: ClassQuery,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/classes/query/unique-references",
            json=classes_query.model_dump(),
        )

    @app.get(
        "/classes/{class_id}",
        tags=["Annotation"],
        responses={
            200: {"model": Class},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(class_id: Id, _=api_integration.global_depends()):
        return await http_client.get_stream_response(f"{base_url}/v1/classes/{class_id}")

    @app.put(
        "/classes/{class_id}",
        tags=["Annotation"],
        responses={
            200: {"model": Class},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Class is locked"},
        },
    )
    async def _(class_id: Id, put_class: PostClass, request: Request, _=api_integration.global_depends()):
        return await http_client.put_stream_response(
            f"{base_url}/v1/classes/{class_id}", data=request.stream(), headers={"Content-Type": "application/json"}
        )

    @app.delete(
        "/classes/{class_id}",
        tags=["Annotation"],
        responses={
            200: {"model": IdObject},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Class is locked"},
        },
    )
    async def _(class_id: Id, _=api_integration.global_depends()):
        return await http_client.delete_stream_response(f"{base_url}/v1/classes/{class_id}")
