from fastapi import Request

from ....models.v1.annotation.primitives import (
    PostPrimitive,
    PostPrimitives,
    Primitive,
    PrimitiveList,
    PrimitiveQuery,
    Primitives,
)
from ....models.v1.commons import Id, IdObject, Message, UniqueReferences
from ....singletons import api_integration, http_client, settings
from ..helpers import params


def add_routes_primitives(app):
    base_url = settings.as_url.rstrip("/")

    @app.get(
        "/primitives",
        tags=["Annotation"],
        responses={
            200: {"model": PrimitiveList},
        },
    )
    async def _(skip: int = None, limit: int = None, _=api_integration.global_depends()):
        return await http_client.get_stream_response(f"{base_url}/v1/primitives", params=params(skip=skip, limit=limit))

    @app.post(
        "/primitives",
        tags=["Annotation"],
        status_code=201,
        responses={201: {"model": Primitives}},
    )
    async def _(
        primitives: PostPrimitives, request: Request, external_ids: bool = False, _=api_integration.global_depends()
    ):
        return await http_client.post_stream_response(
            f"{base_url}/v1/primitives",
            data=request.stream(),
            params=params(external_ids=external_ids),
            headers={"Content-Type": "application/json"},
        )

    @app.put(
        "/primitives/query",
        tags=["Annotation"],
        status_code=200,
        responses={200: {"model": PrimitiveList}},
    )
    async def _(
        primitives_query: PrimitiveQuery,
        skip: int = None,
        limit: int = None,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/primitives/query", json=primitives_query.model_dump(), params=params(skip=skip, limit=limit)
        )

    @app.put(
        "/primitives/query/unique-references",
        tags=["Annotation"],
        status_code=200,
        responses={
            200: {"model": UniqueReferences},
        },
    )
    async def _(
        primitives_query: PrimitiveQuery,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/primitives/query/unique-references",
            json=primitives_query.model_dump(),
        )

    @app.get(
        "/primitives/{primitive_id}",
        tags=["Annotation"],
        responses={
            200: {"model": Primitive},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(primitive_id: Id, _=api_integration.global_depends()):
        return await http_client.get_stream_response(f"{base_url}/v1/primitives/{primitive_id}")

    @app.put(
        "/primitives/{primitive_id}",
        tags=["Annotation"],
        responses={
            200: {"model": Primitive},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Primitive is locked"},
        },
    )
    async def _(primitive_id: Id, primitive: PostPrimitive, request: Request, _=api_integration.global_depends()):
        return await http_client.put_stream_response(
            f"{base_url}/v1/primitives/{primitive_id}",
            data=request.stream(),
            headers={"Content-Type": "application/json"},
        )

    @app.delete(
        "/primitives/{primitive_id}",
        tags=["Annotation"],
        responses={
            200: {"model": IdObject},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Primitive is locked"},
        },
    )
    async def _(primitive_id: Id, _=api_integration.global_depends()):
        return await http_client.delete_stream_response(f"{base_url}/v1/primitives/{primitive_id}")
