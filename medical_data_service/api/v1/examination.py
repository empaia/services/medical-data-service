from ...models.v1.commons import Id, Message
from ...models.v1.examination import (
    Examination,
    ExaminationList,
    ExaminationQuery,
    PostExamination,
    PostScope,
    PutExaminationState,
    Scope,
    ScopeToken,
)
from ...singletons import api_integration, http_client, settings
from .helpers import params


def add_routes_examination(app):
    base_url = settings.es_url.rstrip("/")

    @app.get(
        "/examinations",
        responses={
            200: {"model": ExaminationList},
        },
        tags=["Examination"],
    )
    async def _(
        skip: int = None,
        limit: int = None,
        _=api_integration.global_depends(),
    ):
        return await http_client.get_stream_response(
            f"{base_url}/v1/examinations", params=params(skip=skip, limit=limit)
        )

    @app.post(
        "/examinations",
        responses={
            201: {"model": Examination},
        },
        tags=["Examination"],
    )
    async def _(
        post_examination: PostExamination,
        _=api_integration.global_depends(),
    ):
        return await http_client.post_stream_response(f"{base_url}/v1/examinations", json=post_examination.model_dump())

    @app.put(
        "/examinations/query",
        responses={
            200: {"model": ExaminationList},
        },
        tags=["Examination"],
    )
    async def _(
        query: ExaminationQuery,
        skip: int = None,
        limit: int = None,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/examinations/query", json=query.model_dump(), params=params(skip=skip, limit=limit)
        )

    @app.get(
        "/examinations/{ex_id}",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        ex_id: Id,
        _=api_integration.global_depends(),
    ):
        return await http_client.get_stream_response(f"{base_url}/v1/examinations/{ex_id}")

    @app.get(
        "/jobs/{job_id}/examinations",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        job_id: Id,
        _=api_integration.global_depends(),
    ):
        return await http_client.get_stream_response(f"{base_url}/v1/jobs/{job_id}/examinations")

    @app.put(
        "/examinations/{ex_id}/state",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        ex_id: Id,
        state_update: PutExaminationState,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/examinations/{ex_id}/state", json=state_update.model_dump()
        )

    @app.put(
        "/examinations/{ex_id}/apps/{app_id}/add",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            405: {
                "model": Message,
                "description": "App with given app_id already in examination",
            },
            423: {
                "model": Message,
                "description": "The resource is CLOSED",
            },
        },
        tags=["Examination"],
    )
    async def _(
        ex_id: Id,
        app_id: Id,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(f"{base_url}/v1/examinations/{ex_id}/apps/{app_id}/add")

    @app.put(
        "/examinations/{ex_id}/apps/{app_id}/jobs/{job_id}/add",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            405: {
                "model": Message,
                "description": "Job with given job_id already in examination",
            },
            423: {
                "model": Message,
                "description": "The resource is CLOSED",
            },
        },
        tags=["Examination"],
    )
    async def _(
        ex_id: Id,
        app_id: Id,
        job_id: Id,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(
            f"{base_url}/v1/examinations/{ex_id}/apps/{app_id}/jobs/{job_id}/add"
        )

    @app.delete(
        "/examinations/{ex_id}/jobs/{job_id}",
        responses={
            200: {"model": Examination},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {
                "model": Message,
                "description": "The resource is CLOSED",
            },
        },
        tags=["Examination"],
    )
    async def _(
        ex_id: Id,
        job_id: Id,
        _=api_integration.global_depends(),
    ):
        return await http_client.delete_stream_response(f"{base_url}/v1/examinations/{ex_id}/jobs/{job_id}")

    @app.post(
        "/scopes",
        responses={
            200: {"model": Scope},
            404: {
                "model": Message,
                "description": "The scope was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope: PostScope,
        _=api_integration.global_depends(),
    ):
        return await http_client.post_stream_response(f"{base_url}/v1/scopes/", json=scope.model_dump())

    @app.put(
        "/scopes",
        responses={
            200: {"model": Scope},
            404: {
                "model": Message,
                "description": "The scope was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope: PostScope,
        _=api_integration.global_depends(),
    ):
        return await http_client.put_stream_response(f"{base_url}/v1/scopes/", json=scope.model_dump())

    @app.get(
        "/scopes/{scope_id}",
        responses={
            200: {"model": Scope},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope_id: Id,
        payload=api_integration.global_depends(),
    ):
        return await http_client.get_stream_response(f"{base_url}/v1/scopes/{scope_id}")

    @app.get(
        "/scopes/{scope_id}/token",
        responses={
            200: {"model": ScopeToken},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
        tags=["Examination"],
    )
    async def _(
        scope_id: Id,
        payload=api_integration.global_depends(),
    ):
        return await http_client.get_stream_response(f"{base_url}/v1/scopes/{scope_id}/token")

    @app.get(
        "/examination-service/public-key",
        response_model=bytes,
        responses={
            200: {"description": "Public key for validating Access Tokens created for Jobs"},
        },
        tags=["Examination"],
    )
    async def _(_=api_integration.global_depends()) -> bytes:
        """Get the public key that can be used for validating the Access Tokens created for the Jobs."""
        return await http_client.get_stream_response(f"{base_url}/v1/public-key")
