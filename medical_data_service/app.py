import asyncio
from contextlib import asynccontextmanager

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import __version__ as version
from .api.private_v1 import add_routes_private_v1
from .api.private_v3 import add_routes_private_v3
from .api.root.alive import add_routes_alive
from .api.v1 import add_routes_v1
from .api.v3 import add_routes_v3
from .profiling import add_profiling
from .singletons import http_client, settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI):
    asyncio.create_task(http_client.update_token_routine())
    yield


app = FastAPI(
    debug=settings.debug,
    title="Medical Data Service API",
    version=version,
    description="Medical Data Service API.",
    root_path=settings.root_path,
    openapi_url=openapi_url,
    lifespan=lifespan,
)

app_v1 = FastAPI(openapi_url=openapi_url)
app_v3 = FastAPI(openapi_url=openapi_url)
app_private_v1 = FastAPI(openapi_url=openapi_url)
app_private_v3 = FastAPI(openapi_url=openapi_url)

if settings.cors_allow_origins:
    for app_obj in [app, app_v1, app_v3]:
        app_obj.add_middleware(
            CORSMiddleware,
            allow_origins=settings.cors_allow_origins,
            allow_credentials=settings.cors_allow_credentials,
            allow_methods=["*"],
            allow_headers=["*"],
        )

add_routes_alive(app)
add_routes_v1(app_v1)
add_routes_v3(app_v3)
add_routes_private_v1(app_private_v1)
add_routes_private_v3(app_private_v3)

app.mount("/v1", app_v1)
app.mount("/v3", app_v3)
app.mount("/private/v1", app_private_v1)
app.mount("/private/v3", app_private_v3)

if settings.enable_profiling:
    add_profiling(app)
