from importlib.metadata import version

__version__ = version("medical-data-service")
