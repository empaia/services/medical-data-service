import importlib

from .api.integrations.default import Default


def get_api_integration(settings, logger):
    if settings.api_integration:
        module_name, class_name = settings.api_integration.split(":")
        module = importlib.import_module(module_name)
        IntegrationClass = getattr(module, class_name)
        return IntegrationClass(settings=settings, logger=logger)

    return Default(settings=settings, logger=logger)
