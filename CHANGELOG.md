# 0.17.8 - 11

* renovate
* fixes for case separation

# 0.17.7

* updated mds and models repo (optional original_path field in storage addresses)

# 0.17.6

* fix alive

# 0.17.1

* Extended v3 API for new Job Service routes (see emp-0105)

# 0.17.0

* Added case-id header support for questionnaire responses

# 0.16.7

* updated models submodule

# 0.16.5 & 6

* updated models submodule

# 0.16.4

* fixed missing /fhir prefix for FHIR Resources and FHIR Selectors routes

# 0.16.3

* fixed wrong summaries in API docs

# 0.16.2

* fixed wrong tag in API docs

# 0.16.1

* renovate

# 0.16.0

* added indication and procedure tags to case model to support EMP-0105

# 0.15.0

* added harpy-service to MDS deployment
* added FHIR resource support and selectors/selector taggings

# 0.14.0

* support accept-encoding and content-encoding headers for EMP-0107

# 0.13.0

* added pixelmaps info endpoint to support EMP-0108

# 0.12.1

* updated models
* updated sender_auth

# 0.12.0

* updated annotation-service and added pixelmapd

# 0.11.0

* added case-id header support for EMP-0100
* added api integration hooks for authorization based on case-id

# 0.10.14

* updated ES
* updated tests

# 0.10.6 - 13

* renovate

# 0.10.5

* renovate

# 0.10.4

* added /cases/query endpoint in v3 API

# 0.10.3

* renovate
* using FastAPI lifespan instead of startup event

# 0.10.2

* update models repo
* replaced deprecated dict() with model_dump() - pydantic

# 0.10.1

* update clinical-data-service and models repo

# 0.10.0

* integrated new clinical data service and pluins
* remove storage-mapper
* remove wsi-service

# 0.9.3

* renovate

# 0.9.2

* renovate

# 0.9.1

* splitting private v1 and v3 APIs

# 0.9.0

* added pixelmaps to v3 API

# 0.8.12

* pin pydantic to <2.5

# 0.8.11

* renovate

# 0.8.10

* renovate

# 0.8.9

* renovate

# 0.8.8

* updated for new services

# 0.8.7

* renovate

# 0.8.6

* renovate

# 0.8.5

* renovate

# 0.8.4

* removed old setting

# 0.8.3

* added option to disable post model validation with pydantic v2

# 0.8.2

* renovate

# 0.8.1

* added sender auth client and request timeout
* fixed unused ENVs by adding:
  * chunk_size=settings.connection_chunk_size,
  * connection_limit_per_host=settings.connection_limit_per_host,
* sender auth default cunksize is 10kb

# 0.8.0

* migrate to pydantic v2

# 0.7.22

* renovate

# 0.7.21

* renovate

# 0.7.20

* renovate

# 0.7.19

* renovate

# 0.7.18

* renovate

# 0.7.17

* Added missing endpoint to delete job outputs in v3 API

# 0.7.16

* renovate

# 0.7.15

* renovate

# 0.7.14

* renovate

# 0.7.13

* renovate

# 0.7.12

* renovate

# 0.7.11

* renovate

# 0.7.10

* renovate
* updated annotation service and models

# 0.7.9

* renovate

# 0.7.8

* renovate

# 0.7.7

* renovate

# 0.7.6

* renovate

# 0.7.5

* renovate

# 0.7.4

* renovate

# 0.7.3

* renovate

# 0.7.2

* renamed MDS_DISABLE_POST_MODEL_VALIDATION to MDS_ENABLE_POST_ANNOTATION_MODELS_VALIDATION

# 0.7.1

* renovate

# 0.7.0

* add job input/output validation status routes to v3

# 0.6.13

* renovate

# 0.6.12

* renovate

# 0.6.11

* update models repo

# 0.6.10

* renovate

# 0.6.9

* fixed API naming/path issues

# 0.6.8

* update models repo

# 0.6.7

* update models repo

# 0.6.6

* performance improvements
* updated empaia sender auth

# 0.6.5

* renovate

# 0.6.4

* renamed `app-ui-state` to `app-ui-storage`

# 0.6.3

* added `v3/scopes/{scope_id}/app-ui-state/user` and `v3/scopes/{scope_id}/app-ui-state/scope`

# 0.6.2

* moved more routes to private API spec, that had already been declared as private
* also declared GET /v1/tags as private and moved to private API spec
* deleted GET /v3/tags, because MPS /public/tags route should be used instead

# 0.6.1

* renovate

# 0.6.0

* api v3
  * refactored project structure
  * tests for v3 added

# 0.5.6

* renovate

# 0.5.5

* renovate

# 0.5.4

* updated annotation service and models

# 0.5.3

* add profiling support

# 0.5.1

* renovate

# 0.5.0

* added slide download endpoint
* extended slide info, added format and raw_download

# 0.4.77

* updated Storage-Mapper

# 0.4.76

* renovate
* updated models repo

# 0.4.75

* renovate

# 0.4.74

* updated Annotation-Service

# 0.4.73

* renovate

# 0.4.72

* renovate
* updated Annotation-Service

# 0.4.71

* renovate

# 0.4.70

* renovate

# 0.4.69

* renovate

# 0.4.68

* renovate

# 0.4.67

* renovate

# 0.4.66

* renovate

# 0.4.65

* renovate

# 0.4.64

* renovate

# 0.4.63

* renovate

# 0.4.62

* renovate

# 0.4.61

* renovate

# 0.4.60

* renovate

# 0.4.59

* renovate

# 0.4.58

* renovate

# 0.4.57

* renovate

# 0.4.56

* renovate

# 0.4.55

* renovate

# 0.4.54

* renovate

# 0.4.53

* updated dependencies

# 0.4.52

* updated dependencies

# 0.4.51

* updated ci

# 0.4.50

* added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

# 0.4.49

* allow all origins, which is now possible because frontends do no more use client credentials (instead they
explicitly use an authorization header)

# 0.4.48

* updated all services

# 0.4.47

* updated wsi-service
* updated isyntax-backend

# 0.4.46

* fix for 0.4.45

# 0.4.45

* updated receiver-auth
* added setting to rewrite URL in wellknown

# 0.4.44

* update empaia-receiver-auth
* added new env var MDS_OPENAPI_TOKEN_URL

# 0.4.43

* updated Annotation Service, models and tests

# 0.4.42

* updated JS and models

# 0.4.41

* updated CDS, ES, JS

# 0.4.40

* updated to examination-service 0.6.13

# 0.4.39

* use http_client.get_stream_response() instead of http_client.get() in /v1/scopes/{scope_id}

# 0.4.38

* forwarding the following examination-service routes:
  * route /v1/examinations/{ex_id}/apps/{app_id}/scope to create and retrieve scope id and scope access token
  * route /v1/scopes/{scope_id} to retrieve scope data
  * route /v1/public-key needed for validation of scope access tokens

# 0.4.37

* updated ES

# 0.4.36

* updated services
* added postgres config

# 0.4.35

* configurable token URL path

# 0.4.34

* removed logging timestamps

# 0.4.33

* updated models

# 0.4.32

* updated CDS, SMS
* added delete route to SMS

# 0.4.31

* deprecated result tree routes
* updated AS

# 0.4.30

* updated JS

# 0.4.29

* updated CDS, AS and models

# 0.4.28

* updated AS

# 0.4.27

* updated WSI

# 0.4.26

* updated WSI

# 0.4.25

* integrated receiver auth class

# 0.4.24

* updated CDS
  * get tags from definitions

# 0.4.23

* updated JS
* updated AS
* added timestamps to logger

# 0.4.22

* GET case
* GET cases
  *?with_slides
* POST cases
* POST slides
  * ?external_ids
* compose + settings
  * allow_external_ids

# 0.4.21

* updated services to fix race condition when using a unified DB

# 0.4.20

* unified DB

# 0.4.19

* updated ES, JS, CDS
* updated ES tests

# 0.4.18

* changed isyntax-backend registry url

# 0.4.17

* updated job-service

# 0.4.16

* updated services, routes, models and tests
* all MDS services now use PSQL (replacing MongoDB for job- and clinical-data-service)

# 0.4.15

* updated models

# 0.4.14

* updated AS and ES version

# 0.4.13

* added new ES endpoints
* updated ES version
* updated ES tests

# 0.4.12

* updated sender-auth

# 0.4.11

* updated sender-auth

# 0.4.10

* updated sender-auth with ProxySettings

# 0.4.9

* WSI Service update
* CDS Update
* POST Slide or Case to CDS now return complete object instead of just the generated ID

# 0.4.8

* added new route to get job token

# 0.4.7

* added DELETE job route

# 0.4.6

* updated dependencies

# 0.4.5

* updated dependencies

# 0.4.4

* updated dependencies

# 0.4.3

* added query-job route

# 0.4.2

* update models for custom validation of reference_type

# 0.4.1

* use empaia-sender-auth lib instead of custom stream-client

# 0.4.0

* Updated annotation service to version 0.13.0
* Added new result tree route `GET /v1/jobs/{job_id}/tree/primitives/{primitive_id}/details`

# 0.3.11

* added exception handling to stream client so aiohttp session always gets closed

# 0.3.10

* renamed Slide models in clinical.py with prefix Clinical to avoid name clash

# 0.3.9

* fixed *broken_pipe* error in streaming client

# 0.3.8

* added API Documentation for Job Service and Clinical Data Service

# 0.3.7

* updated job models

# 0.3.6

* updated annotation models to always use Enum instead of Literal for reference_type
* updated job models documentation

# 0.3.5

* update parameters of wsi-service routes

# 0.3.4

* allow partial service configuration

# 0.3.3

* changed integration plugin interface

# 0.3.2

* alive route now checks micro-services

# 0.3.1

* generic auth plugin interface

# 0.3.0

* kraken to FastAPI migration
* unified API
